#include <TH1F.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TROOT.h>
#include <TChain.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TLegend.h>

#include <map>

#include <stdlib.h>
#include "JES_BalanceFitter.h"
#include <vector>


class MyFitter 
{
 public:
  MyFitter(const std::string name, const std::string filename, const std::string optionsi, bool doTruth, bool doMC,  std::string systematic);
  ~MyFitter();
  double GetNbinsPt(){return m_nBinsPt;}
  double GetNbinsEta(){return m_nBinsEta;}
  Double_t* GetPtBinBoundaries(){return m_ptBinBoundaries;}
  Double_t* GetEtaBinBoundaries(){return m_etaBinBoundaries;}
  Double_t  GetMeanAtEtaPtBin(const int EtaBin, const int PtBin);
  Double_t  GetMeanErrorAtEtaPtBin(const int EtaBin, const int PtBin);
  Double_t  GetSigmaAtEtaPtBin(const int EtaBin, const int PtBin);
  Double_t  GetSigmaErrorAtEtaPtBin(const int EtaBin, const int PtBin);
  bool GetDebugFlag() {return m_debug;}
  
 private:
  Double_t GetValuesEtaPtBin
    (const int EtaBin, const int PtBin, 
     const std::map<std::pair<int, int>, std::pair<double, double> >& themap, 
     const bool checkError);
  void SetBranchAddresses();
  void DecodeOptions(TString options);
  void DecodeBinning(TString binning_str, Double_t* binning_array, Int_t& n_binning);
  bool passJVT(float pt, float eta, float JVT, float JVTCut);
  float ChannelWeight(int channel);

  Double_t m_etaBinBoundaries[BUFSIZ];
  Double_t m_ptBinBoundaries[BUFSIZ];
  Int_t    m_nBinsEta;
  Int_t    m_nBinsPt;
  Float_t  m_responseHistoMax;
  Float_t  m_responseHistoMin;
  Int_t    m_responseHistoNbins;
  bool     m_debug;
  bool     m_doTruth;
  bool 	   m_doMC;
  std::string m_syst_flag;


  TFile* m_file;
  //TTree* m_tree;
  TChain* m_tree;
  const double m_GeVUnit;
  
  std::string m_name;
  
  /*
  // branches
  Int_t   runNumber;
  Int_t   eventNumber;
  Int_t   lumiBlock;
  Int_t   bcid;
  Int_t   NPV;
  Float_t actualInteractionsPerCrossing;
  Float_t averageInteractionsPerCrossing;
  Float_t weight;
  Float_t weight_xs;
  Float_t weight_prescale;
  Bool_t  PassOR;
  Bool_t  PassDR;
  Bool_t  PassJetQuality;
  Bool_t  PassJetPt;
  Bool_t  PassJetEta;
  Bool_t  PassDeltaEta;
  Float_t DeltaEtaPhotonJet;
  Float_t DeltaPhiPhotonJet;
  Float_t DeltaRPhotonClosestJet;
  Float_t PhotonTopoEtCone40;
  Int_t   PhotonAuthor;
  Int_t   PhotonConversionType;
  Float_t PhotonEta;
  Float_t PhotonPhi;
  Float_t PhotonPt;
  Float_t PhotonE;
  Float_t PhotonSF;
  Float_t PhotonSFUp;
  Float_t PhotonSFDw;
  Float_t JetEta;
  Float_t JetPhi;
  Float_t JetPt;
  Float_t JetE;
  Int_t   JetConeTruthLabelID;
  Float_t JetEtaEMScale;
  Float_t PhotonJetEta;
  Float_t PhotonJetPhi;
  Float_t PhotonJetPt;
  Float_t PhotonJetM;
  Float_t SubLeadingJetPt;
  Float_t SubLeadingJetEta;
  Float_t SubLeadingJetPhi;
  Float_t SubLeadingJetM;
  Float_t dPhiPhotonJet;
  Float_t pTRef;
  Int_t   JetIsFix60;
  Int_t   JetIsFix70;
  Int_t   JetIsFix77;
  Int_t   JetIsFix85;
  Int_t   JetIsFlt70;

  */

   // Declaration of leaf types
  /* Int_t           runNumber;
   Int_t           eventNumber;
   Int_t           lumiBlock;
   UInt_t          coreFlags;
   Int_t           mcEventNumber;
   Int_t           mcChannelNumber;
   Float_t         mcEventWeight;
   Int_t           passL1;
   Int_t           passHLT;
   Int_t           masterKey;
   Int_t           lvl1PrescaleKey;
   Int_t           hltPrescaleKey;
   vector<string>  *passedTriggers;
   vector<float>   *triggerPrescales;
   Int_t           njets;
   vector<float>   *jet_E;
   vector<float>   *jet_pt;
   vector<float>   *jet_phi;
   vector<float>   *jet_eta;
   vector<float>   *jet_HECFrac;
   vector<float>   *jet_EMFrac;
   vector<float>   *jet_CentroidR;
   vector<float>   *jet_FracSamplingMax;
   vector<float>   *jet_FracSamplingMaxIndex;
   vector<float>   *jet_LowEtConstituentsFrac;
   vector<float>   *jet_GhostMuonSegmentCount;
   vector<float>   *jet_Width;
   vector<float>   *jet_emScalePt;
   vector<float>   *jet_constScalePt;
   vector<float>   *jet_pileupScalePt;
   vector<float>   *jet_originConstitScalePt;
   vector<float>   *jet_etaJESScalePt;
   vector<float>   *jet_gscScalePt;
   vector<float>   *jet_insituScalePt;
   vector<float>   *jet_SV0;
   vector<float>   *jet_SV1;
   vector<float>   *jet_IP3D;
   vector<float>   *jet_SV1IP3D;
   vector<float>   *jet_MV1;
   vector<float>   *jet_MV2c00;
   vector<float>   *jet_MV2c20;
   vector<int>     *jet_HadronConeExclTruthLabelID;
   Int_t           njets_mv2c20_Fix70;
   vector<int>     *jet_MV2c20_isFix70;
   vector<vector<float> > *jet_MV2c20_SFFix70;
   vector<float>   *weight_jet__MV2c20_SFFix70;
   Int_t           njets_mv2c20_Fix85;
   vector<int>     *jet_MV2c20_isFix85;
   vector<vector<float> > *jet_MV2c20_SFFix85;
   Int_t           nph;
   vector<float>   *ph_pt;
   vector<float>   *ph_phi;
   vector<float>   *ph_eta;
   vector<float>   *ph_m;
   vector<float>   *ph_E;
   vector<int>     *ph_isIsolated_Cone40CaloOnly;
   vector<int>     *ph_isIsolated_Cone40;
   vector<int>     *ph_isIsolated_Cone20;
   vector<float>   *ph_ptcone20;
   vector<float>   *ph_ptcone30;
   vector<float>   *ph_ptcone40;
   vector<float>   *ph_ptvarcone20;
   vector<float>   *ph_ptvarcone30;
   vector<float>   *ph_ptvarcone40;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_topoetcone30;
   vector<float>   *ph_topoetcone40;
   Int_t           nph_IsLoose;
   vector<int>     *ph_IsLoose;
   Int_t           nph_IsMedium;
   vector<int>     *ph_IsMedium;
   Int_t           nph_IsTight;
   vector<int>     *ph_IsTight;

*/

   std::vector<float>   *jet_pt;
  std::vector<float>   *jet_phi;
  std::vector<float>   *jet_eta;
  std::vector<float>   *jet_E;
  std::vector<float>   *ph_pt;
  std::vector<float>   *ph_E;
  std::vector<float>   *ph_eta;
  std::vector<float>   *ph_phi;
  std::vector<char>    *jet_isFix85;
  std::vector<char>    *jet_isFix70;
  std::vector<int>    *ph_IsTight;
  std::vector<int>    *jet_PartonTruthLabelID;
  Long64_t      eventNumber;
  std::vector<std::string>  *passedTriggers;
  std::vector<float> *weight_jetSFFix85;
  std::vector<float> *weight_jetSFFix70;
  std::vector<float> *jet_Jvt;
  Float_t   mcEventWeight;
  Int_t     mcChannelNumber;




  std::map<std::pair<int, int>, TH1F*> m_inclusiveHistos;
  std::map<std::pair<int, int>, TH1F*> m_isFix85Histos;
  std::map<std::pair<int, int>, TH1F*> m_isFix70Histos;
  std::map<std::pair<int, int>, TH1F*> m_truthHistos;
  
  std::map<std::pair<int, int>, TH1F*> m_lightTruth_inc;
  std::map<std::pair<int, int>, TH1F*> m_cTruth_inc;
  std::map<std::pair<int, int>, TH1F*> m_bTruth_inc;
  std::map<std::pair<int, int>, TH1F*> m_lightTruth_70;
  std::map<std::pair<int, int>, TH1F*> m_cTruth_70;
  std::map<std::pair<int, int>, TH1F*> m_bTruth_70;
  std::map<std::pair<int, int>, TH1F*> m_lightTruth_85;
  std::map<std::pair<int, int>, TH1F*> m_cTruth_85;
  std::map<std::pair<int, int>, TH1F*> m_bTruth_85;


  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_inclusive;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_isFix70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_isFix85;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_truth;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_lightTruth_inc;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_lightTruth_70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_lightTruth_85;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_cTruth_inc;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_cTruth_70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_cTruth_85;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_bTruth_inc;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_bTruth_70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_mean_bTruth_85;

  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_inclusive;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_isFix70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_isFix85;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_lightTruth_inc;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_lightTruth_70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_lightTruth_85;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_cTruth_inc;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_cTruth_70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_cTruth_85;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_bTruth_inc;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_bTruth_70;
  std::map<std::pair<int, int>, std::pair<double, double> > m_fit_sigma_bTruth_85;
  
  int GetBinNumber(const double& value, const Double_t* boudaries, const int& nbins);
  int GetEtaBinNumber(const double& value);
  int GetPtBinNumber(const double& value);
  void SetHistograms(std::map<std::pair<int, int>, TH1F*>& histoMap, std::string flag);
  void Loop();
  void SaveHistograms(std::map<std::pair<int, int>, TH1F*> histoMap, std::string flag);
  void FitHistograms(std::map<std::pair<int, int>, TH1F*>& histoMap, std::map<std::pair<int, int>, std::pair<double, double> >& meanMap, std::string flag);
  void PlotMeans(std::map<std::pair<int, int>, std::pair<double, double> >& meanMap, std::string flag);
  void doTurnOnCurve();
};
