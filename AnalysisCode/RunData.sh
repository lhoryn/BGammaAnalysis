make -f Makefile.Analysis

#ptbin="ptbin=0,80,110,150,200,250,300,350,400" # [0--150], [150-200], [200-250], [250-300], [300-350], [400]
ptbin="ptbin=0,25,30,55,65,75,85,105,125,145,160,210,260,310,400,600,800,1000" # [0--150], [150-200], [200-250], [250-300], [300-350], [400]
etabin="etabin=0.0,0.8,2.0" # [0.0,0.8], [0.8,2.0], [2.0]

# to do debug
#./Analysis.exe "${ptbin} ${etabin} histomax=5.0 histomin=0.0 histonbins=1000 debug"

./Analysis.exe --binning "${ptbin} ${etabin} histomax=5.0 histomin=0.0 histonbins=1000" --files locations/data_trees_path.txt --datatype data --systflag nominal


