#include "boost/program_options.hpp"
#include <TFile.h>
#include <TH1.h>
#include <TGraphErrors.h>
#include "JES_BalanceFitter.h"
#include <TF1.h>
#include <string>
int main(int argc, char* argv[]){





  //Input # of Pt bin, and words for beginning/end of file, matches output from MyFitter.cxx
  namespace po = boost::program_options;
  po::options_description desc("Options");
  
  int PtBin;
  std::string nameflag;
  std::string btagflag;
  bool replace = false;

  desc.add_options()
	  ("help","produce help message")
	  ("PtBin", po::value<int> (&PtBin), "Pt Bin, ex 00")
	  ("Name", po::value<std::string>  (&nameflag), "beginning of histo name, ex pythia_nominal")
	  ("btag", po::value<std::string>  (&btagflag), "end of histo name, ex isFix85")
          ("replace", po::bool_switch(&replace), "replace old mean with refit one");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
   
  //Load histogram to be refitted
  std::string file = nameflag + "_output_" + btagflag + ".root";
  TFile f(file.c_str());
  std::string histoname;
  if(PtBin < 10)  histoname = nameflag+ "_Eta00_Pt0" + std::to_string(PtBin) + "_" + btagflag;
  else histoname = nameflag+ "_Eta00_Pt" + std::to_string(PtBin) + "_"+ btagflag;
  //f.Close();
  
  std::cout << histoname << std::endl;

  TH1F* toFix = (TH1F*)f.Get(histoname.c_str());


  //Refit and print output 
  double NsigmaForFit = 1.0;
  JES_BalanceFitter *myFitter = new JES_BalanceFitter(NsigmaForFit);
  myFitter->SetPoisson();

  TF1* fit = myFitter->Fit(toFix,-1);
  
  std::cout << "Refitted histo:" << std::endl;
  std::cout << "\tMean " << myFitter->GetMean() << std::endl;
  std::cout << "\tError " << myFitter->GetMeanError() << std::endl << std::endl;
  
  //compare with stored mean/error value from MyFitter.cxx
  TFile m(Form("%s_mean_graphs_%s.root",nameflag.c_str(),btagflag.c_str()),"UPDATE");
  TGraphErrors* orig = (TGraphErrors*)m.Get("0.0<|eta|<0.8");
  double orig_mean;
  double orig_pt;
  orig->GetPoint(PtBin,orig_pt,orig_mean);
  double orig_error = orig->GetErrorY(PtBin);
  double x_width = orig->GetErrorX(PtBin);
  
  std::cout << "Original Fit: " << std::endl;
  std::cout << "\tMean " << orig_mean << std::endl;
  std::cout << "\tError " << orig_error << std::endl << std::endl;
  
  //Replace old value with new value
  if(replace){
	  orig->SetPoint(PtBin,orig_pt,myFitter->GetMean());
	  orig->SetPointError(PtBin,x_width,myFitter->GetMeanError());
	  std::cout << "You did it!" << std::endl;
  }
  
  //Double check output
  double fix;
  double pt;
  double err = orig->GetErrorY(PtBin);
  double xw = orig->GetErrorX(PtBin);
  orig->GetPoint(PtBin,pt,fix);
  std::cout << "New Stored Value: " << std::endl;
  std::cout << "\tMean " << fix << " Pt " << pt << std::endl;
  std::cout << "\tError " << err << " x width " << xw << std::endl;

  
  orig->Write();
  m.Write();





  

}
