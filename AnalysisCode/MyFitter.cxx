#include <TCanvas.h>
#include <TStyle.h>
#include <TChain.h>
#include <TFileCollection.h>
#include <TCollection.h>
#include <TH2.h>

#include "MyFitter.h"
#include "JES_BalanceFitter.h"
#include<iostream>

//=========================================
MyFitter::MyFitter(const std::string name, const std::string filename, const std::string options, bool doTruth, bool doMC, std::string systematic) : 
  m_responseHistoMax(2.0), m_responseHistoMin(0.0), m_responseHistoNbins(1000),
  m_file(0), m_tree(0), m_GeVUnit(1000.)
{
  gStyle->SetOptFit(1111);
  m_name = name + "_" + systematic;
  m_doTruth = doTruth;
  m_doMC = doMC;
  m_inclusiveHistos.clear();
  m_isFix70Histos.clear();
  m_isFix85Histos.clear();
  m_fit_mean_inclusive.clear();
  m_fit_mean_isFix70.clear();
  m_fit_mean_isFix85.clear();
  m_syst_flag = systematic; 
  m_debug = false;

/*  m_file = TFile::Open(filename.c_str());
  if (!m_file) {
    fprintf(stderr, "ERROR %s cannnot be open [%d]\n", filename.c_str(), __LINE__);
    exit(EXIT_FAILURE);
  }
  
  m_tree = (TTree*)m_file->Get("GammaB");
*/
  
  std::string treeName;
  
  if(m_syst_flag == "nominal" || m_syst_flag == "JVT_up" || m_syst_flag == "JVT_down" || m_syst_flag == "dPhi_up" || m_syst_flag == "dPhi_down" || m_syst_flag == "subJet_up" || m_syst_flag == "subJet_down")
	  treeName = "GammaB/nominal";
  else
	  treeName = "GammaB/" + m_syst_flag;

  m_tree = new TChain(treeName.c_str());
  TFileCollection* fc = new TFileCollection("dum","",filename.c_str());
  m_tree->AddFileInfoList((TCollection*)fc->GetList());

  std::cout << "number of entries " << m_tree->GetEntries() << std::endl;
  

  if (!m_tree)  {
    fprintf(stderr, "ERROR tree cannnot be found in %s [%d]\n", filename.c_str(), __LINE__);
    exit(EXIT_FAILURE);    
  }

  
  SetBranchAddresses();
  std::cout << "set branch addresses" << std::endl;
  DecodeOptions(options);
 

  SetHistograms(m_inclusiveHistos,"inclusive");
  SetHistograms(m_isFix85Histos,"isFix85");
  SetHistograms(m_isFix70Histos,"isFix70");
  if(m_doTruth){
	  SetHistograms (m_truthHistos,"truth");
	  SetHistograms(m_lightTruth_inc,"lightTruth_inc");
	  SetHistograms(m_cTruth_inc,"cTruth_inc");
	  SetHistograms(m_bTruth_inc,"bTruth_inc");
	  SetHistograms(m_lightTruth_70,"lightTruth_70");
	  SetHistograms(m_cTruth_70,"cTruth_70");
	  SetHistograms(m_bTruth_70,"bTruth_70");
	  SetHistograms(m_lightTruth_85,"lightTruth_85");
	  SetHistograms(m_cTruth_85,"cTruth_85");
	  SetHistograms(m_bTruth_85,"bTruth_85");
	
  
  }
  

  Loop();

  FitHistograms(m_inclusiveHistos,m_fit_mean_inclusive,"inclusive");
  FitHistograms(m_isFix85Histos,m_fit_mean_isFix85,"isFix85");
  FitHistograms(m_isFix70Histos,m_fit_mean_isFix70,"isFix70");
  if(m_doTruth){
	  FitHistograms(m_truthHistos,m_fit_mean_truth,"truth");
	  FitHistograms(m_lightTruth_inc,m_fit_mean_lightTruth_inc,"lightTruth_inc");
	  FitHistograms(m_cTruth_inc,m_fit_mean_cTruth_inc,"cTruth_inc");
	  FitHistograms(m_bTruth_inc,m_fit_mean_bTruth_inc,"bTruth_inc");
	  FitHistograms(m_lightTruth_70,m_fit_mean_lightTruth_70,"lightTruth_70");
	  FitHistograms(m_cTruth_70,m_fit_mean_cTruth_70,"cTruth_70");
	  FitHistograms(m_bTruth_70,m_fit_mean_bTruth_70,"bTruth_70");
	  FitHistograms(m_lightTruth_85,m_fit_mean_lightTruth_85,"lightTruth_85");
	  FitHistograms(m_cTruth_85,m_fit_mean_cTruth_85,"cTruth_85");
	  FitHistograms(m_bTruth_85,m_fit_mean_bTruth_85,"bTruth_85");
  }

  SaveHistograms(m_inclusiveHistos,"inclusive");
  SaveHistograms(m_isFix85Histos,"isFix85");
  SaveHistograms(m_isFix70Histos,"isFix70");
  if(m_doTruth){
	  SaveHistograms(m_truthHistos,"truth");
	  SaveHistograms(m_lightTruth_inc,"lightTruth_inc");
	  SaveHistograms(m_cTruth_inc,"cTruth_inc");
	  SaveHistograms(m_bTruth_inc,"bTruth_inc");
	  SaveHistograms(m_lightTruth_70,"lightTruth_70");
	  SaveHistograms(m_cTruth_70,"cTruth_70");
	  SaveHistograms(m_bTruth_70,"bTruth_70");
	  SaveHistograms(m_lightTruth_85,"lightTruth_85");
	  SaveHistograms(m_cTruth_85,"cTruth_85");
	  SaveHistograms(m_bTruth_85,"bTruth_85");
  }

  PlotMeans(m_fit_mean_inclusive, "inclusive");
  PlotMeans(m_fit_mean_isFix85,"isFix85");
  PlotMeans(m_fit_mean_isFix70,"isFix70");
  if(m_doTruth){
	  PlotMeans(m_fit_mean_truth,"truth");
	  PlotMeans(m_fit_mean_lightTruth_inc,"lightTruth_inc");
	  PlotMeans(m_fit_mean_cTruth_inc,"cTruth_inc");
	  PlotMeans(m_fit_mean_bTruth_inc,"bTruth_inc");
	  PlotMeans(m_fit_mean_lightTruth_70,"lightTruth_70");
	  PlotMeans(m_fit_mean_cTruth_70,"cTruth_70");
	  PlotMeans(m_fit_mean_bTruth_70,"bTruth_70");
	  PlotMeans(m_fit_mean_lightTruth_85,"lightTruth_85");
	  PlotMeans(m_fit_mean_cTruth_85,"cTruth_85");
	  PlotMeans(m_fit_mean_bTruth_85,"bTruth_85");
  }

}

//=========================================
MyFitter::~MyFitter()
{
//  m_file->Close();
}

//=========================================
void MyFitter::SetBranchAddresses()
{

/*  
  m_tree->SetBranchAddress("runNumber", &runNumber);
  m_tree->SetBranchAddress("eventNumber", &eventNumber);
  m_tree->SetBranchAddress("lumiBlock", &lumiBlock);
  m_tree->SetBranchAddress("bcid", &bcid);
  m_tree->SetBranchAddress("NPV", &NPV);
  m_tree->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing);
  m_tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  m_tree->SetBranchAddress("weight", &weight);
  m_tree->SetBranchAddress("weight_xs", &weight_xs);
  m_tree->SetBranchAddress("weight_prescale", &weight_prescale);
  m_tree->SetBranchAddress("PassOR", &PassOR);
  m_tree->SetBranchAddress("PassDR", &PassDR);
  m_tree->SetBranchAddress("PassJetQuality", &PassJetQuality);
  m_tree->SetBranchAddress("PassJetPt", &PassJetPt);
  m_tree->SetBranchAddress("PassJetEta", &PassJetEta);
  m_tree->SetBranchAddress("PassDeltaEta", &PassDeltaEta);
  m_tree->SetBranchAddress("DeltaEtaPhotonJet", &DeltaEtaPhotonJet);
  m_tree->SetBranchAddress("DeltaPhiPhotonJet", &DeltaPhiPhotonJet);
  m_tree->SetBranchAddress("DeltaRPhotonClosestJet", &DeltaRPhotonClosestJet);
  m_tree->SetBranchAddress("PhotonTopoEtCone40", &PhotonTopoEtCone40);
  m_tree->SetBranchAddress("PhotonAuthor", &PhotonAuthor);
  m_tree->SetBranchAddress("PhotonConversionType", &PhotonConversionType);
  m_tree->SetBranchAddress("PhotonEta", &PhotonEta);
  m_tree->SetBranchAddress("PhotonPhi", &PhotonPhi);
  m_tree->SetBranchAddress("PhotonPt", &PhotonPt);
  m_tree->SetBranchAddress("PhotonE", &PhotonE);
  m_tree->SetBranchAddress("PhotonSF", &PhotonSF);
  m_tree->SetBranchAddress("PhotonSFUp", &PhotonSFUp);
  m_tree->SetBranchAddress("PhotonSFDw", &PhotonSFDw);
  m_tree->SetBranchAddress("JetEta", &JetEta);
  m_tree->SetBranchAddress("JetPhi", &JetPhi);
  m_tree->SetBranchAddress("JetPt", &JetPt);
  m_tree->SetBranchAddress("JetE", &JetE);
  m_tree->SetBranchAddress("JetConeTruthLabelID", &JetConeTruthLabelID);
  m_tree->SetBranchAddress("JetEtaEMScale", &JetEtaEMScale);
  m_tree->SetBranchAddress("PhotonJetEta", &PhotonJetEta);
  m_tree->SetBranchAddress("PhotonJetPhi", &PhotonJetPhi);
  m_tree->SetBranchAddress("PhotonJetPt", &PhotonJetPt);
  m_tree->SetBranchAddress("PhotonJetM", &PhotonJetM);
  m_tree->SetBranchAddress("SubLeadingJetPt", &SubLeadingJetPt);
  m_tree->SetBranchAddress("SubLeadingJetEta", &SubLeadingJetEta);
  m_tree->SetBranchAddress("SubLeadingJetPhi", &SubLeadingJetPhi);
  m_tree->SetBranchAddress("SubLeadingJetM", &SubLeadingJetM);
  m_tree->SetBranchAddress("dPhiPhotonJet", &dPhiPhotonJet);
  m_tree->SetBranchAddress("pTRef", &pTRef);
  m_tree->SetBranchAddress("JetIsFix60", &JetIsFix60);
  m_tree->SetBranchAddress("JetIsFix70", &JetIsFix70);
  m_tree->SetBranchAddress("JetIsFix77", &JetIsFix77);
  m_tree->SetBranchAddress("JetIsFix85", &JetIsFix85);
  m_tree->SetBranchAddress("JetIsFlt70", &JetIsFlt70);  
*/
   jet_pt = 0;
   jet_phi = 0;
   jet_eta = 0;
   jet_E = 0;
   ph_E = 0;
   ph_pt = 0;
   ph_phi = 0;
   ph_eta = 0;
   eventNumber = 0;
   jet_isFix85 = 0;
   jet_isFix70 = 0;
   ph_IsTight = 0;
   jet_PartonTruthLabelID = 0;
   passedTriggers = 0;
   weight_jetSFFix85 = 0;
   weight_jetSFFix70 = 0;
   jet_Jvt = 0;
   mcEventWeight = 0;
   mcChannelNumber = 0;

/*	m_tree->SetBranchAddress("jet_pt", jet_pt, &b_jet_pt);
	m_tree->SetBranchAddress("jet_eta", jet_eta, &b_jet_pt);
	m_tree->SetBranchAddress("jet_phi", jet_phi, &b_jet_phi);
	m_tree->SetBranchAddress("ph_phi", ph_phi, &b_ph_phi);
	m_tree->SetBranchAddress("ph_pt", ph_pt, &b_ph_pt);
  */
	m_tree->SetBranchAddress("jet_pt", &jet_pt);
	m_tree->SetBranchAddress("jet_eta", &jet_eta);
	m_tree->SetBranchAddress("jet_phi", &jet_phi);
	m_tree->SetBranchAddress("jet_E", &jet_E);
	m_tree->SetBranchAddress("ph_phi", &ph_phi);
	m_tree->SetBranchAddress("ph_pt", &ph_pt);
	m_tree->SetBranchAddress("ph_eta", &ph_eta);
	m_tree->SetBranchAddress("ph_E", &ph_E);
	m_tree->SetBranchAddress("eventNumber", &eventNumber);
	m_tree->SetBranchAddress("jet_isFix85", &jet_isFix85);
	m_tree->SetBranchAddress("jet_isFix70", &jet_isFix70);
	m_tree->SetBranchAddress("ph_IsTight", &ph_IsTight);
	m_tree->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID);
	m_tree->SetBranchAddress("passedTriggers", &passedTriggers);
	m_tree->SetBranchAddress("weight_jetSFFix85", &weight_jetSFFix85);
	m_tree->SetBranchAddress("weight_jetSFFix70", &weight_jetSFFix70);
	m_tree->SetBranchAddress("jet_Jvt", &jet_Jvt);
	m_tree->SetBranchAddress("mcEventWeight", &mcEventWeight);
	m_tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber);


}

//=========================================
void MyFitter::DecodeOptions(TString options)
{
  TString tok;
  Ssiz_t  from = 0;
  
  m_nBinsEta=0;
  m_nBinsPt=0;
  
  while (options.Tokenize(tok, from, " ")) {
    if (tok.BeginsWith("etabin=")) {
      tok.ReplaceAll("etabin=", "");
      DecodeBinning(tok, m_etaBinBoundaries, m_nBinsEta);
    }
    else if (tok.BeginsWith("ptbin=")) {
      tok.ReplaceAll("ptbin=", "");
      DecodeBinning(tok, m_ptBinBoundaries, m_nBinsPt);
    }
    else if (tok=="debug") {
      m_debug = true;
    }
    else if (tok.BeginsWith("histomax=")) {
      tok.ReplaceAll("histomax=", "");
      m_responseHistoMax = strtof(tok.Data(), NULL);
    }
    else if (tok.BeginsWith("histomin=")) {
      tok.ReplaceAll("histomin=", "");
      m_responseHistoMin = strtof(tok.Data(), NULL);
    }
    else if (tok.BeginsWith("histonbins=")) {
      tok.ReplaceAll("histonbins=", "");
      m_responseHistoNbins = strtof(tok.Data(), NULL);
    }
    else {
      fprintf(stderr, "undefined option = %s", tok.Data());
      exit(EXIT_FAILURE);
    }
      
  }
  
  if (m_nBinsEta==0 || m_nBinsPt==0) {
    fprintf(stderr, "please set eta and pT binning in options, currently m_nBinsEta==%d m_nBinsPt=%d \n",
	    m_nBinsEta, m_nBinsPt);
    exit(EXIT_FAILURE);
  }
  
  printf("Info> m_nBinsEta=%d m_nBinsPt=%d\n", m_nBinsEta, m_nBinsPt);
}

//=========================================
void MyFitter::DecodeBinning(TString binning_str, Double_t* binning_array, Int_t& n_binning)
{
  TString tok;
  Ssiz_t  from = 0;
  binning_str.ReplaceAll(" ", "");
  int iArrayIndex=0;
  while (binning_str.Tokenize(tok, from, ",")) {  
    const double x = strtod(tok.Data(), NULL);
    binning_array[iArrayIndex]=x;
    iArrayIndex++;
  }
  n_binning=iArrayIndex;
  
  return;
}

//=========================================
int MyFitter::GetBinNumber(const double& value, const Double_t* boudaries, const int& nbins)
{
  int iBin=0;
  for (; iBin<nbins; iBin++) {
    if (value<boudaries[iBin+1]) break;
  }
  return (iBin<=nbins-1 ? iBin : nbins-1);
}

//=========================================
int MyFitter::GetEtaBinNumber(const double& value)
{
  return GetBinNumber(value, m_etaBinBoundaries, m_nBinsEta);
}

//=========================================
int MyFitter::GetPtBinNumber(const double& value)
{
  return GetBinNumber(value, m_ptBinBoundaries, m_nBinsPt);
}
//=========================================





//=========================================
bool MyFitter::passJVT(float pt, float eta, float JVT, float JVTCut)
{
	if(pt < 50 && TMath::Abs(eta) < 2.4 && JVT < JVTCut)
		return false;

	return true;
}
//=========================================


//=========================================
float MyFitter::ChannelWeight(int channel)
{

	float weight = 1;

	//pythia
/*	if(channel==423103) weight = 0.00145251285453;
	if(channel==423104) weight = 0.000332866481549;
	if(channel==423105) weight = 0.000020555488452;
	if(channel==423106) weight = 0.00000160530650431;
	if(channel==423100) weight = 0.224801293945;
	if(channel==423101) weight = 0.00156672374039;
	if(channel==423102) weight = 0.000345091290455;
	if(channel==423103) weight = 0.00145251285453;
	*/


	if(channel==423110) weight = 1.19119205913e-09;
	if(channel==423108) weight = 2.629126867e-07;
	if(channel==423109) weight = 1.48344324424e-08;
	if(channel==423099) weight = 1.85131201137;
	if(channel==423104) weight = 0.000332643991508;
	if(channel==423105) weight = 1.93653431424e-05;
	if(channel==423106) weight = 1.2646107968e-06;
	if(channel==423107) weight = 8.78082881017e-07;
	if(channel==423100) weight = 0.218729113634;
	if(channel==423101) weight = 0.0155283484052;
	if(channel==423102) weight = 0.000340893607888;
	if(channel==423103) weight = 0.00145251285453;
	if(channel==423112) weight = 9.00269191402e-12;
	if(channel==423111) weight = 9.89249745677e-11;

	//sherpa
	if(channel==361046) weight = 5.34580703515e-05;
	if(channel==361048) weight = 4.46876193743e-06;
	if(channel==361049) weight = 1.27946921202e-05;
	if(channel==361060) weight = 1.00296457459e-12;
	if(channel==361047) weight = 3.20829451728e-05;
	if(channel==361044) weight = 0.000743191620243;
	if(channel==361045) weight = 8.88186804949e-06;
	if(channel==361042) weight = 0.000624051160835;
	if(channel==361043) weight = 0.000754152871203;
	if(channel==361040) weight = 0.00169897326153;
	if(channel==361041) weight = 0.00168866815884;
	if(channel==361039) weight = 0.000717522091756;
	if(channel==361061) weight = 6.47645033084e-13;
	if(channel==361058) weight = 7.39132396735e-10;
	if(channel==361055) weight = 1.70462267404e-07;
	if(channel==361054) weight = 6.77427193613e-08;
	if(channel==361057) weight = 2.86154413456e-10;
	if(channel==361056) weight = 2.99110514512e-07;
	if(channel==361051) weight = 1.71541210002e-06;
	if(channel==361050) weight = 1.98408642381e-05;
	if(channel==361053) weight = 6.91736840901e-06;
	if(channel==361052) weight = 4.31591205624e-06;
	if(channel==361062) weight = 3.75476994943e-13;


	return weight;

}
//=========================================




//=========================================
void MyFitter::Loop()
{
  printf("Number of Events: %i\n",m_tree->GetEntries());
  
  //cut flow vars
  int c_total = m_tree->GetEntries();
  int c_jetPhoton = 0;
  int c_onePhoton = 0;
  int c_subleadingJet = 0;
  int c_dPhi = 0;
  int c_isFix85 = 0;
  int c_isFix70 = 0;
  int c_inclusive = 0;
  int c_trigger = 0;
  int c_truth = 0;
  
  int n_sub7 = 0;
  int n_ev7 = 0;
  int n_sub_tot = 0;
  int n_tot = 0;

  TH1F* cutflow = new TH1F("cutflow_30to55GeV","cutflow_40to55GeV",12,0,12);
  TH1F* cutflowall = new TH1F("cutflow","cutflow",12,0,12);
  TH1F* pt_refweight = new TH1F("pt_refweight","pt_refweight",100,0,1000);
  TH1F* pt_jetweight = new TH1F("pt_jetweight","pt_jetweight",100,0,1000);
  TH1F* pt_refpresel = new TH1F("pt_refpresel","pt_refpresel",100,0,1000);
  TH1F* pt_jetpresel = new TH1F("pt_jetpresel","pt_jetpresel",100,0,1000);
  TH1F* debugJVTpt = new TH1F("debugJVTpt","lead jet pt of events with empty jvt",100,0,1000);
  TH1I* debugJVTnjets = new TH1I("debugJVTnjets","number of jets in events with empty jvt",15,0,15);
  
  std::map<std::pair<int, int>, TH1F*> ptjetweight_Histos;
  SetHistograms(ptjetweight_Histos,"ptjetweight_Histos");
  std::map<std::pair<int, int>, TH1F*> ptrefweight_Histos;
  SetHistograms(ptrefweight_Histos,"ptrefweight_Histos");
  
  for (int iEntry=0; iEntry<m_tree->GetEntries(); iEntry++) {
	  m_tree->GetEntry(iEntry);

	  cutflowall->Fill("total",1);

	  if(iEntry % 100000 == 0) printf("Event Number: %i\n",iEntry);

	  if(jet_pt->size() == 0 || ph_pt->size() == 0) continue;
	  c_jetPhoton++;
	  cutflowall->Fill("at least one jet, photon",1);
          
          if(m_debug) std::cout << "onejet one photon " << std::endl;

	  //Nomincal cuts
	  float phiCut = 2.8;
	  double subJetCut = 15;
	  double subJetFrac = .1;
	  double subPhotonCut = 20;
	  float weight = 1;
	  float JVT = 0.64;
	  if(m_doMC) 
		  weight = mcEventWeight * ChannelWeight(mcChannelNumber);
	  
	  //Systematics
	  if(m_syst_flag == "dPhi_up") 
		  phiCut = 2.9;
	  if(m_syst_flag == "dPhi_down") 
		  phiCut = 2.7;
	  if(m_syst_flag == "subJet_up"){
		  subJetCut = 20;
		  subJetFrac = .15;
	  }
	  if(m_syst_flag == "subJet_down"){
		  subJetCut = 10;
		  subJetFrac = .05;
	  }
	  if(m_syst_flag == "JVT_up")
		  JVT = .92;
	  if(m_syst_flag == "JVT_down")
		  JVT = .14;

	  
	  float JetPt = 0;
	  float JetPhi = 0;
	  float JetEta = 0;
	  float SubLeadingJetPt = 0;
	  int iLeadingJet = 0;
	  
	  if(jet_Jvt->size() == 0){
		  debugJVTpt->Fill(jet_pt->at(0));
		  debugJVTnjets->Fill(jet_pt->size());
	  }

	  if(jet_Jvt->size() == 0){
		  continue;
	  }
	  //Jet Definitions
	  int goodJetCount = 0;
	  for(int i=0; i<jet_pt->size(); i++){
		  
		  if( goodJetCount == 2) break;

		  //jets are in order, so the first one that passes is the lead jet
		  if( passJVT(jet_pt->at(i),jet_eta->at(i),jet_Jvt->at(i),JVT) ){
			  if(goodJetCount == 0){
				  JetPt = jet_pt->at(i);
				  JetPhi = jet_phi->at(i);
				  JetEta = jet_eta->at(i);
				  iLeadingJet = i;
			  }
			  if(goodJetCount == 1){
				  SubLeadingJetPt = jet_pt->at(i);
			  }

			  goodJetCount++;
		  }
	  }

	  if(m_debug) std::cout << "good jet def" << std::endl;

	  //if no good jets, skip
	  if(goodJetCount == 0)  continue;
	  //cutflow->Fill("JVT",1);
	  
	  if(JetPt>30 && JetPt<55) cutflow->Fill("total",1);
          cutflowall->Fill("good jet", 1);

	  //Object Definitions
	  float PhotonPhi = ph_phi->at(0);
	  float dPhi = TMath::Abs(JetPhi-PhotonPhi);
	  float pTRef = ph_pt->at(0) * TMath::Abs(TMath::Cos(dPhi));
	  float PhotonPt = ph_pt->at(0);
          
	  pt_jetpresel->Fill(JetPt,weight);
	  pt_refpresel->Fill(pTRef,weight);


	  if(ph_IsTight->at(0) == 0) continue;
	  if(JetPt>30 && JetPt<55) cutflow->Fill("tight photon",1);
          cutflowall->Fill("tight photon", 1);
          if(m_debug) std::cout << "tight photon " << std::endl;
	  

	  //cut if second good photon with high pT
	  if(ph_pt->size() > 1 && ph_IsTight->at(1)==1 && ph_pt->at(1) > subPhotonCut) continue;
	  c_onePhoton++; 
	  if(JetPt>30 && JetPt<55) cutflow->Fill("subleading photon",1);
	  cutflowall->Fill("subleading photon",1);
	  if(m_debug) std::cout << "subleading photon" << std::endl;
	  
	  //Subleading Jet cut
	  if(jet_pt->size() > 1 && SubLeadingJetPt > std::max(subJetCut, subJetFrac * pTRef)) continue;
	  c_subleadingJet++;
	  if(JetPt>30 && JetPt<55) cutflow->Fill("subleading jet",1);
	  cutflowall->Fill("subleading jet",1);
	  if(m_debug) std::cout << "subleading jet " << std::endl;
	  
	  // back-to-back choice with delta Phi Cut
	  if ( TMath::ACos(TMath::Cos(dPhi)) < phiCut ) continue;
	  c_dPhi++;
	  if(JetPt>30 && JetPt<55) cutflow->Fill("dPhi",1);
	  cutflowall->Fill("dPhi",1);
	  if(m_debug) std::cout << "dPhi" << std::endl;
	  
	  //Trigger requirement
	  if(PhotonPt > 25 && PhotonPt < 30 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g20_loose_L1EM12") == passedTriggers->end()) continue;
	  if(PhotonPt > 30 && PhotonPt < 35 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g25_loose_L1EM15") == passedTriggers->end()) continue;
	  //if(PhotonPt > 40 && PhotonPt < 45 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g40_loose_L1EM15") == passedTriggers->end()) continue;
	  //if(PhotonPt > 45 && PhotonPt < 50 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g45_loose_L1EM15") == passedTriggers->end()) continue;
	  if(PhotonPt > 40 && PhotonPt < 45 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_25_loose_L1EM15") == passedTriggers->end()) continue;
	  if(PhotonPt > 45 && PhotonPt < 50 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_25_loose_L1EM15") == passedTriggers->end()) continue;
	  if(PhotonPt > 50 && PhotonPt < 55 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g25_loose_L1EM15") == passedTriggers->end()) continue;
	  if(PhotonPt > 55 && PhotonPt < 65 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g50_loose_L1EM15") == passedTriggers->end()) continue;
	  if(PhotonPt > 65 && PhotonPt < 75 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g60_loose") == passedTriggers->end()) continue;
	  if(PhotonPt > 75 && PhotonPt < 85 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g70_loose") == passedTriggers->end()) continue;
	  if(PhotonPt > 85 && PhotonPt < 105 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g80_loose") == passedTriggers->end()) continue;
	  if(PhotonPt > 105 && PhotonPt < 125 && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g100_loose") == passedTriggers->end()) continue;
	  if(PhotonPt > 125 && ( find(passedTriggers->begin(),passedTriggers->end(),"HLT_g120_loose") == passedTriggers->end() && find(passedTriggers->begin(),passedTriggers->end(),"HLT_g140_loose") == passedTriggers->end() )) continue;
	  c_trigger++;
	  if(JetPt>40 && JetPt<55) cutflow->Fill("trigger",1);
	  cutflowall->Fill("trigger",1);
	  if(m_debug) std::cout << "trigger"<< std::endl;


	  const int iBinEta = GetEtaBinNumber(TMath::Abs(JetEta));
	  const int iBinPt  = GetPtBinNumber(pTRef);
	  std::pair<int, int> binningPair(iBinEta, iBinPt);

	  if (m_debug) {printf("iBinEta=%d (%f) iBinPt=%d (%f) \n",
			  iBinEta, JetEta,iBinPt, pTRef);}
	  pt_refweight->Fill(pTRef,weight);
	  pt_jetweight->Fill(JetPt,weight);
	  ptjetweight_Histos[binningPair]->Fill(JetPt,weight);
	  ptrefweight_Histos[binningPair]->Fill(pTRef,weight);

	 /* if(SubLeadingJetPt !=0) n_sub_tot++;
	  n_tot++;
	  if(TMath::Abs(JetEta) < 0.8 && JetPt/pTRef <= 0.7){
		  std::cout << "****Low Tail, printing info:" << std::endl;
		  std::cout << "    Jet Pt    : " << JetPt << std::endl;
		  std::cout << "    SubJet Pt : " << SubLeadingJetPt << std::endl;
		  std::cout << "    Photon Pt : " << PhotonPt << std::endl;
		  std::cout << "    Jet Phi   : " << JetPhi << std::endl;
		  std::cout << "    Photon Phi: " << PhotonPhi << std::endl;
		  std::cout << "    dPhi      : " << dPhi << std::endl;
                  std::cout << "    Pt Ref    : " << pTRef << std::endl;

		  if(SubLeadingJetPt != 0) n_sub7++;
		  n_ev7++;
	  }
*/
	  //Inclusive
	  m_inclusiveHistos[binningPair]->Fill(JetPt/pTRef,weight);
	  c_inclusive++;
	  if(JetPt>40 && JetPt<55) cutflow->Fill("inclusive",1);
	  cutflowall->Fill("inclusive",1);

	  //Inclusive truth
	  if(m_doTruth){
		  if(jet_PartonTruthLabelID->at(iLeadingJet) == 5)  	m_bTruth_inc[binningPair]->Fill(JetPt/pTRef,weight);
		  if(jet_PartonTruthLabelID->at(iLeadingJet) == 4)	m_cTruth_inc[binningPair]->Fill(JetPt/pTRef,weight);
		  if(jet_PartonTruthLabelID->at(iLeadingJet) == 1 || jet_PartonTruthLabelID->at(iLeadingJet) == 2 || jet_PartonTruthLabelID->at(iLeadingJet) == 3 || jet_PartonTruthLabelID->at(iLeadingJet) == 9 || jet_PartonTruthLabelID->at(iLeadingJet) == 21)	m_lightTruth_inc[binningPair]->Fill(JetPt/pTRef,weight);
	  }

	  //85% btag
	  if(jet_isFix85->at(iLeadingJet)){
		  if(m_doMC)  weight = weight * weight_jetSFFix85->at(iLeadingJet);
		  m_isFix85Histos[binningPair]->Fill(JetPt/pTRef,weight);
		  c_isFix85++;
		  if(JetPt>40 && JetPt<55) cutflow->Fill("85 btag",1);
		  cutflowall->Fill("85 btag",1);
		  if(m_debug) std::cout << "85 btag " << std::endl;
		  if(m_doTruth){
			  if(jet_PartonTruthLabelID->at(iLeadingJet) == 5)  	m_bTruth_85[binningPair]->Fill(JetPt/pTRef,weight);
			  if(jet_PartonTruthLabelID->at(iLeadingJet) == 4)	m_cTruth_85[binningPair]->Fill(JetPt/pTRef,weight);
			  if(jet_PartonTruthLabelID->at(iLeadingJet) == 1 || jet_PartonTruthLabelID->at(iLeadingJet) == 2 || jet_PartonTruthLabelID->at(iLeadingJet) == 3 || jet_PartonTruthLabelID->at(iLeadingJet) == 9 || jet_PartonTruthLabelID->at(iLeadingJet) == 21)	m_lightTruth_85[binningPair]->Fill(JetPt/pTRef,weight);
		  }
	  }

	  

	  //70% b-tag
	  if(jet_isFix70->at(iLeadingJet)){
		  if(m_doMC)  weight = weight * weight_jetSFFix70->at(iLeadingJet);
		  m_isFix70Histos[binningPair]->Fill(JetPt/pTRef,weight);
		  c_isFix70++;
		  if(JetPt>40 && JetPt<55) cutflow->Fill("70 btag",1);
		  cutflowall->Fill("70 btag",1);
		  if(m_debug) std::cout << "70 btag" << std::endl;
		  if(m_doTruth){
			  if(jet_PartonTruthLabelID->at(iLeadingJet) == 5)  	m_bTruth_70[binningPair]->Fill(JetPt/pTRef,weight);
			  if(jet_PartonTruthLabelID->at(iLeadingJet) == 4)	m_cTruth_70[binningPair]->Fill(JetPt/pTRef,weight);
			  if(jet_PartonTruthLabelID->at(iLeadingJet) == 1 || jet_PartonTruthLabelID->at(iLeadingJet) == 2 || jet_PartonTruthLabelID->at(iLeadingJet) == 3 || jet_PartonTruthLabelID->at(iLeadingJet) == 9 || jet_PartonTruthLabelID->at(iLeadingJet) == 21)	m_lightTruth_70[binningPair]->Fill(JetPt/pTRef,weight);
		  }
	  }

	  //Truth bTagged, this is redundant
	  if(m_doTruth){
		  if(jet_PartonTruthLabelID->at(iLeadingJet) == 5){
			  m_truthHistos[binningPair]->Fill(JetPt/pTRef,weight);
			  c_truth++;
			  cutflow->Fill("truth",weight);
		  }
	  }
	  if(!m_doTruth)   cutflow->Fill("truth",0);
	  



  }
  
 // std::cout << "@@@@ number with no subjet in tail " << n_sub7 << " total number in tail " << n_ev7 << std::endl;
 // std::cout << "@@@@ number with no subjet total   " << n_sub_tot << " total number " << n_tot << std::endl;


  SaveHistograms(ptjetweight_Histos,"ptjetweight_Histos");
  SaveHistograms(ptrefweight_Histos,"ptrefweight_Histos");

  TFile f(Form("%s_cutflow_update4055_triggers.root", m_name.c_str()), "RECREATE");
  cutflow->Write();
  cutflowall->Write();
  pt_refweight->Write();
  pt_jetweight->Write();
  pt_refpresel->Write();
  pt_jetpresel->Write();
  debugJVTpt->Write();
  debugJVTnjets->Write();
  f.Write();


}

//=======================================
void MyFitter::SetHistograms(std::map<std::pair<int, int>, TH1F*>& histoMap , std::string flag)
{
  for (int iEta=0; iEta<m_nBinsEta; iEta++) {
    std::string titleEta = ((iEta==m_nBinsEta-1) ? Form("|#eta|>%.1f", m_etaBinBoundaries[iEta]) : 
			    Form("%.1f<|#eta|<%.1f", m_etaBinBoundaries[iEta], m_etaBinBoundaries[iEta+1]));
    
    for (int iPt=0; iPt<m_nBinsPt; iPt++) {
      std::pair<int, int> binningPair(iEta, iPt);
      std::string titlePt = ((iPt==m_nBinsPt-1) ? Form("p_{T}>%.1f", m_ptBinBoundaries[iPt]) : 
			     Form("%.1f<p_{T}<%.1f", m_ptBinBoundaries[iPt], m_ptBinBoundaries[iPt+1]));
      
      if (m_debug) { printf("(eta, pt) bin = (%d, %d)\n", iEta, iPt); }
      TH1F* h;
      if(flag == "ptjetweight_Histos" || flag == "ptrefweight_Histos" || flag == "ptjetnoweight_Histos" || flag == "ptrefnoweight_Histos")
	      h = new TH1F(Form("%s_Eta%02d_Pt%02d_%s", m_name.c_str(), iEta, iPt, flag.c_str()), Form("%s %s (%s)", titleEta.c_str(), titlePt.c_str(), flag.c_str()), 100,0,1000);
      else
	      h = new TH1F(Form("%s_Eta%02d_Pt%02d_%s", m_name.c_str(), iEta, iPt, flag.c_str()), Form("%s %s (%s)", titleEta.c_str(), titlePt.c_str(), flag.c_str()), m_responseHistoNbins, m_responseHistoMin, m_responseHistoMax);
      histoMap.insert(std::pair<std::pair<int, int>, TH1F*>(binningPair, h));
    }
  }
}

//=========================================
void MyFitter::SaveHistograms(std::map<std::pair<int, int>, TH1F*> histoMap, std::string flag)
{
  TCanvas c1;
  c1.Print(Form("%s_output_%s.pdf[", m_name.c_str(),flag.c_str()));
  for (int iEta=0; iEta<m_nBinsEta; iEta++) {
    for (int iPt=0; iPt<m_nBinsPt; iPt++) {
      std::pair<int, int> binningPair(iEta, iPt);
      histoMap[binningPair]->Draw();
      c1.Print(Form("%s_output_%s.pdf", m_name.c_str(),flag.c_str()));
    }
  }
  c1.Print(Form("%s_output_%s.pdf]", m_name.c_str(),flag.c_str()));
  
  TFile f(Form("%s_output_%s.root", m_name.c_str(),flag.c_str()), "RECREATE");
  for (int iEta=0; iEta<m_nBinsEta; iEta++) {
    for (int iPt=0; iPt<m_nBinsPt; iPt++) {
      std::pair<int, int> binningPair(iEta, iPt);
      histoMap[binningPair]->Write();
    }
  }
  f.Write();
}


//=========================================
void  MyFitter::FitHistograms(std::map<std::pair<int, int>, TH1F*>& histoMap, std::map<std::pair<int, int>, std::pair<double, double>>& meanMap,  std::string flag)
{
  double NsigmaForFit = 1.0;
  JES_BalanceFitter *myFitter = new JES_BalanceFitter(NsigmaForFit);
  myFitter->SetPoisson();  
  

  TCanvas c1;
  TFile f(Form("%s_output_fit_%s.root", m_name.c_str(),flag.c_str()), "RECREATE");
  c1.Print(Form("%s_output_fit_%s.pdf[", m_name.c_str(),flag.c_str()));  
  for (int iEta=0; iEta<m_nBinsEta; iEta++) {
    for (int iPt=0; iPt<m_nBinsPt; iPt++) {
      std::pair<int, int> binningPair(iEta, iPt);
      TH1F* h = histoMap[binningPair];
      double fitMin = -1;
      TF1 *fitted = myFitter->FitAndDraw(h,fitMin);
     // fitted->SetTitle(Form("Fit_%s_Eta%02d_Pt%02d_%s", m_name.c_str(), iEta, iPt, flag.c_str()));
      c1.Print(Form("%s_output_fit_%s.pdf", m_name.c_str(),flag.c_str()));  
      const double mean       = myFitter->GetMean();
      const double mean_error = myFitter->GetMeanError();
      const double sigma       = myFitter->GetSigma();
      const double sigma_error = myFitter->GetSigmaError();
      meanMap[binningPair]  = std::pair<double, double>(mean, mean_error);
   //   m_fit_sigma[binningPair] = std::pair<double, double>(sigma, sigma_error);
      fitted->Write();
    }
  }
  f.Write();
  c1.Print(Form("%s_output_fit_%s.pdf]", m_name.c_str(),flag.c_str()));  
}

//=========================================
Double_t MyFitter::GetValuesEtaPtBin
(const int EtaBin, const int PtBin, 
 const std::map<std::pair<int, int>, std::pair<double, double> >& themap, 
 const bool checkError)
{
  std::pair<int, int> binning(EtaBin, PtBin);
  if (themap.find(binning)==m_fit_mean_inclusive.end()) {
    fprintf(stderr, "out of range EtaBin=%d PtBin=%d \n", EtaBin, PtBin);
    exit(EXIT_FAILURE);
  }
  if (checkError) {
    return themap.find(binning)->second.second;
  } else {
    return themap.find(binning)->second.first;
  }
}

//=========================================
Double_t MyFitter::GetMeanAtEtaPtBin(const int EtaBin, const int PtBin)
{
  return GetValuesEtaPtBin(EtaBin, PtBin, m_fit_mean_inclusive, false);
}

//=========================================
Double_t MyFitter::GetMeanErrorAtEtaPtBin(const int EtaBin, const int PtBin)
{
  return GetValuesEtaPtBin(EtaBin, PtBin, m_fit_mean_inclusive, true);
}

//=========================================
Double_t MyFitter::GetSigmaAtEtaPtBin(const int EtaBin, const int PtBin)
{
  return GetValuesEtaPtBin(EtaBin, PtBin, m_fit_sigma_inclusive, false);
}

//=========================================
Double_t MyFitter::GetSigmaErrorAtEtaPtBin(const int EtaBin, const int PtBin)
{
  return GetValuesEtaPtBin(EtaBin, PtBin, m_fit_sigma_inclusive, true);
}

//=========================================
void MyFitter::PlotMeans(std::map<std::pair<int, int>, std::pair<double, double> >& meanMap, std::string flag){
  
	
  std::vector<TGraphErrors*> dataGraphs;
  dataGraphs.clear();
  
  TLegend leg(0.6, 0.6, 0.88, 0.88);
  TLegend leg_diff(0.6, 0.6, 0.88, 0.88);
  for (int iEta=0; iEta<m_nBinsEta; iEta++) {
    std::string titleEta = 
      ((iEta==m_nBinsEta-1) ? Form("|eta|>%.1f", m_etaBinBoundaries[iEta]) : 
       Form("%.1f<|eta|<%.1f", m_etaBinBoundaries[iEta], m_etaBinBoundaries[iEta+1]));
    
    if (m_debug) { printf("debug> pass line %d\n", __LINE__); }
    
    TGraphErrors* data_graph = new TGraphErrors();
    data_graph->SetName((titleEta.c_str()));
    data_graph->SetMarkerStyle(iEta+24);
    data_graph->SetMarkerColor(kBlack);
    data_graph->SetLineColor(kBlack);
    
    leg.AddEntry(data_graph, Form("%s", titleEta.c_str()), "PE");
    
    for (int iPt=0; iPt<m_nBinsPt; iPt++) {
      std::pair<int, int> binningPair(iEta, iPt);

      const double xCenter = (m_ptBinBoundaries[iPt+1] - m_ptBinBoundaries[iPt]<0) ? m_ptBinBoundaries[iPt]+200 : (m_ptBinBoundaries[iPt] + m_ptBinBoundaries[iPt+1])/2.;
      const double xError  = (m_ptBinBoundaries[iPt+1] - m_ptBinBoundaries[iPt]<0) ? m_ptBinBoundaries[iPt]   : (m_ptBinBoundaries[iPt+1] - m_ptBinBoundaries[iPt])/2.; // for the last bin
      const double dataY      = meanMap.find(binningPair)->second.first;
      const double dataYError = meanMap.find(binningPair)->second.second;
      
      data_graph->SetPoint(iPt, xCenter, dataY);
      data_graph->SetPointError(iPt, xError, dataYError);
    
    }
    dataGraphs.push_back(data_graph);
  }
  
  
  TFile f(Form("%s_mean_graphs_%s.root",m_name.c_str(),flag.c_str()), "RECREATE");
  for (int iEta=0; iEta<m_nBinsEta; iEta++) {
    dataGraphs.at(iEta)->Write();
  }
  leg.Write();
  f.Write();

}
