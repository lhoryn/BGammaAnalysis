#include "MyFitter.h"
#include <string>

#include <TGraphErrors.h>
#include <vector>
#include <TLegend.h>
#include <TCanvas.h>
#include <TLatex.h>

#include "boost/program_options.hpp"

int main(int argc, char* argv[])
{
  if (argc!=2) {
    printf("usage> %s 'options'\n", argv[0]);
  }

  
  namespace po = boost::program_options;
  po::options_description desc("Options");
  
  std::string filelist;
  std::string type;
  std::string systname;
  std::string options;

  desc.add_options()
	  ("help","produce help message")
	  ("binning", po::value<std::string> (&options), "binning info")
	  ("files", po::value<std::string> (&filelist), "list of paths to root files")
	  ("datatype", po::value<std::string>  (&type), "data, pythia, sherpa")
	  ("systflag", po::value<std::string> (&systname), "nominal, dPhi_up, dPhi_down, subJet_up, subJet_down, JVT_up, JVT_down, EG_SCALE_ALL__1up, EG_SCALE_ALL__1down, EG_RESOLUTION_ALL__1up, EG_RESOLUTION_ALL__1down, JET_GroupedNP_1__1up, JET_GroupedNP_1__1down, JET_GroupedNP_2__1up, JET_GroupedNP_2__1down, JET_GroupedNP_3__1up, JET_GroupedNP_3__1down");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  bool doTruth = false;  //Pythia nominal only
  bool doMC = false;
  if(type == "sherpa" || type == "pythia") doMC = true;
  if(type == "pythia" && systname == "nominal") doTruth = true;


  std::cout << " data type " << type << std::endl;
  std::cout << " sytematic name " << systname << std::endl;
  std::cout << " file list " << filelist << std::endl;
  std::cout << " doTruth " << doTruth << std::endl;
  std::cout << " doMC " << doMC << std::endl;
  std::cout << " options " << options << std::endl;

  //label, file, options, dotruth, domc,  syst flag
  MyFitter Fitter(type, filelist, options, doTruth, doMC,  systname);  
  
  
  /*  
  const Double_t* ptBinBoundaries  = sherpaFitter.GetPtBinBoundaries();
  const Double_t* etaBinBoundaries = sherpaFitter.GetEtaBinBoundaries();
  
  std::vector<TGraphErrors*> sherpaGraphs;
  
  TLegend leg(0.6, 0.6, 0.88, 0.88);
  TLegend leg_diff(0.6, 0.6, 0.88, 0.88);
  for (int iEta=0; iEta<nBinsEta; iEta++) {
    std::string titleEta = 
      ((iEta==nBinsEta-1) ? Form("|#eta|>%.1f", etaBinBoundaries[iEta]) : 
       Form("%.1f<|#eta|<%.1f", etaBinBoundaries[iEta], etaBinBoundaries[iEta+1]));
    if (debug) { printf("debug> pass line %d\n", __LINE__); }
    TGraphErrors* sherpa_graph = new TGraphErrors();
    sherpa_graph->SetName((titleEta.c_str()));
    sherpa_graph->SetMarkerStyle(iEta+24);
    sherpa_graph->SetMarkerColor(kBlack);
    sherpa_graph->SetLineColor(kBlack);
    
    sherpaGraphs.push_back(sherpa_graph);
    leg.AddEntry(sherpa_graph, Form("%s", titleEta.c_str()), "PE");
    
    for (int iPt=0; iPt<nBinsPt; iPt++) {
      const double xCenter = (ptBinBoundaries[iPt+1] - ptBinBoundaries[iPt]<0) ? ptBinBoundaries[iPt]*2 : (ptBinBoundaries[iPt] + ptBinBoundaries[iPt+1])/2.;
      const double xError  = (ptBinBoundaries[iPt+1] - :ptBinBoundaries[iPt]<0) ? ptBinBoundaries[iPt]   : (ptBinBoundaries[iPt+1] - ptBinBoundaries[iPt])/2.; // for the last bin
      const double sherpaY      = sherpaFitter.GetMeanAtEtaPtBin(iEta, iPt);
      const double sherpaYError = sherpaFitter.GetMeanErrorAtEtaPtBin(iEta, iPt);
      sherpa_graph->SetPoint(iPt, xCenter, sherpaY);
      sherpa_graph->SetPointError(iPt, xError, sherpaYError);
    }
  }
  
  
  TFile f("peak_output_sherpa_photon.root", "RECREATE");
  for (int iEta=0; iEta<nBinsEta; iEta++) {
    sherpaGraphs.at(iEta)->Write();
  }
  leg.Write();
  f.Write();

  */
}
