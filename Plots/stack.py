import rootpy.ROOT as ROOT
from rootpy.io import root_open
from rootpy.plotting import *
from rootpy.plotting.style import *
#from rootpy.plotting import Hist, Hist2D, Graph, Canvas, Legend, Color
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.io as io
from rootpy.plotting.views import *

#full pythia analysis
pythia_inclusive = root_open("root_files/pythia_nominal_output_inclusive.root")
pythia_isFix70 = root_open("root_files/pythia_nominal_output_isFix70.root")
pythia_isFix85 = root_open("root_files/pythia_nominal_output_isFix85.root")

#data analysis
data_inclusive = root_open("root_files/data_nominal_output_inclusive.root")
data_isFix70 = root_open("root_files/data_nominal_output_isFix70.root")
data_isFix85 = root_open("root_files/data_nominal_output_isFix85.root")

#stacking analysis
inclusive_light = root_open("root_files/pythia_nominal_output_lightTruth_inc.root")
inclusive_b = root_open("root_files/pythia_nominal_output_bTruth_inc.root")
inclusive_c = root_open("root_files/pythia_nominal_output_cTruth_inc.root")

isFix70_light = root_open("root_files/pythia_nominal_output_lightTruth_70.root")
isFix70_b = root_open("root_files/pythia_nominal_output_bTruth_70.root")
isFix70_c = root_open("root_files/pythia_nominal_output_cTruth_70.root")

isFix85_light = root_open("root_files/pythia_nominal_output_lightTruth_85.root")
isFix85_b = root_open("root_files/pythia_nominal_output_bTruth_85.root")
isFix85_c = root_open("root_files/pythia_nominal_output_cTruth_85.root")

outFolder = "./outplot_systematics/truthBins/"

set_style('ATLAS')

canvas = Canvas()

eta_range = ['00','01','02']
pt_range = ['00','01','02','03','04','05','06','07','08']

pt_vals = {'00' : '0 < p_{T}^{jet} [GeV] < 80', 
	'01' : '80 < p_{T}^{jet} [GeV] < 110', 
	'02' : '110 < p_{T}^{jet} [GeV] < 150',
	'03' : '150 < p_{T}^{jet} [GeV] < 200',
	'04' : '200 < p_{T}^{jet} [GeV] < 250',
	'05' : '250 < p_{T}^{jet} [GeV] < 300', 
	'06' : '300 < p_{T}^{jet} [GeV] < 350', 
	'07' : '350 < p_{T}^{jet} [GeV] < 400',
	'08': 'p_{T}^{jet} [GeV] > 400' }

eta_vals ={'00' : '0 < |#eta| < 0.8',
	 '01' : '0.8 < |#eta| < 2.0',
	 '02' : '|#eta| > 2.0'}



def DrawStackDataMC(hist_light,hist_c,hist_b,hist_mc,hist_data,eta,pt,scale,flag):

	canvas.Clear()

	#set histograms for stack
	hist_light.Rebin(5)
	hist_light.Scale(scale)
	hist_light.fillstyle = 'solid'
	hist_light.linewidth = 0
	hist_light.fillcolor = ROOT.kCyan+2
	hist_light.linecolor = ROOT.kCyan+2

	hist_c.Rebin(5)
	hist_c.Scale(scale)
	hist_c.fillstyle = 'solid'
	hist_c.linewidth = 0
	hist_c.fillcolor = ROOT.kBlue-2
	hist_c.linecolor = ROOT.kBlue-2

	hist_b.Rebin(5)
	hist_b.Scale(scale)
	hist_b.fillstyle = 'solid'
	hist_b.linewidth = 0
	hist_b.fillcolor = ROOT.kViolet-3
	hist_b.linecolor = ROOT.kViolet-3

	#create stack
	stack = HistStack()
	stack.Add(hist_b)
	stack.Add(hist_c)
	stack.Add(hist_light)

	#set histogram styles
	hist_mc.Rebin(5)
	hist_data.Rebin(5)
	hist_mc.Scale(scale)
	hist_mc.linewidth = 2

	hist_mc.GetXaxis().SetRangeUser(0,2.5)
	hist_mc.SetStats(0)
	hist_data.SetStats(0)
	hist_light.SetStats(0)
	hist_c.SetStats(0)
	hist_b.SetStats(0)

	#set legend and labels and draw
	legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
	legend.AddEntry(hist_mc, "MC", style = 'l')
	legend.AddEntry(hist_data, "Data", style ='p')
	legend.AddEntry(hist_light, "Light jets", style = 'f')
	legend.AddEntry(hist_c, "c jets", style = 'f')
	legend.AddEntry(hist_b, "b jets", style = 'f')

	hist_mc.xaxis.SetTitle("P_{T}^{jet} / P_{T}^{ref}")

	hist_mc.Draw("hist")
	stack.Draw("histsame")
	hist_data.Draw("pe0same")
	legend.Draw("same")

	label = ROOT.TText(0.675, 0.75, 'ATLAS')
	label.SetTextFont(73)
	label.SetTextSize(25)
	label.SetNDC()
	label.Draw()

	label2 = ROOT.TText(0.775, 0.75, 'Internal')
	label2.SetTextFont(43)
	label2.SetTextSize(25)
	label2.SetNDC()
	label2.Draw()

	label3 = ROOT.TLatex(0.7, 0.7, pt_vals[pt])
	label3.SetTextFont(43)
	label3.SetTextSize(20)
	label3.SetNDC()
	label3.Draw()

	label4 = ROOT.TLatex(0.7, 0.65, eta_vals[eta])
	label4.SetTextFont(43)
	label4.SetTextSize(20)
	label4.SetNDC()
	label4.Draw()

	label5 = ROOT.TText(0.7, 0.60, flag)
	label5.SetTextFont(43)
	label5.SetTextSize(20)
	label5.SetNDC()
	label5.Draw()


	canvas.Update()

	canvas.SaveAs(outFolder + "Eta_" + eta + "_Pt_" + pt + "_" + flag + ".pdf")



def DrawStacks(file_light,file_c,file_b,file_mc,file_data,flag):

	#adjust for stupid naming choice
	if flag == "inclusive":
		file_flag = "inc"
	if flag == "isFix70":
		file_flag = "70"
	if flag == "isFix85":
		file_flag = "85"


	for eta in eta_range:
		for pt in pt_range:

			#set truth content stack
			hist_light = file_light.Get('pythia_nominal_Eta' + eta + '_Pt' + pt + '_lightTruth_' + file_flag )
			hist_c = file_c.Get('pythia_nominal_Eta' + eta + '_Pt' + pt + '_cTruth_' + file_flag)
			hist_b = file_b.Get('pythia_nominal_Eta' + eta + '_Pt' + pt + '_bTruth_' + file_flag)
		

			#draw with data and MC
			hist_mc = file_mc.Get('pythia_nominal_Eta' + eta + '_Pt' + pt + '_' + flag)
			hist_data = file_data.Get('data_nominal_Eta' + eta + '_Pt' + pt + '_' + flag)

			weight = (hist_data.Integral() / hist_mc.Integral())

			#truthStack = StackHists(hist_light,hist_c,hist_b,weight)
			DrawStackDataMC(hist_light,hist_c,hist_b,hist_mc,hist_data,eta,pt,weight,flag)




#Draw inclusive
DrawStacks(inclusive_light,inclusive_c,inclusive_b,pythia_inclusive,data_inclusive,"inclusive")

#Draw isFix70
DrawStacks(isFix70_light,isFix70_c,isFix70_b,pythia_isFix70,data_isFix70,"isFix70")

#Draw isFix85
DrawStacks(isFix85_light,isFix85_c,isFix85_b,pythia_isFix85,data_isFix85,"isFix85")
