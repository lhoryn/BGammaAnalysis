import rootpy.ROOT as ROOT
from rootpy.io import root_open
from rootpy.plotting import *
from rootpy.plotting.style import *
#from rootpy.plotting import Hist, Hist2D, Graph, Canvas, Legend, Color
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from rootpy.tree import Tree, TreeChain
from math import sqrt


out_folder = "./outplot_systematics/"

set_style('ATLAS')

canvas = Canvas()

eta_vals = ['0.0<|eta|<0.8', '0.8<|eta|<2.0','|eta|>2.0']
eta_strings = eta_vals ={'0.0<|eta|<0.8' : '0 < |#eta| < 0.8',
	 '0.8<|eta|<2.0' : '0.8 < |#eta| < 2.0',
	 '|eta|>2.0' : '|#eta| > 2.0'}



def GetPercentDiffGraph(graph_nominal,graph_sys):

	n_points = graph_nominal.GetN()
	graph_diff = Graph(n_points)

	for i in range(0,n_points):
		x_nominal = ROOT.Double()
		y_nominal = ROOT.Double()
		x_sys = ROOT.Double()
		y_sys = ROOT.Double()
		diff = ROOT.Double()
		errory = ROOT.Double()
		errorx = ROOT.Double()

		nominal = graph_nominal.GetPoint(i,x_nominal,y_nominal)
		sys = graph_sys.GetPoint(i,x_sys,y_sys)
		diff = ( abs(y_nominal - y_sys) / y_nominal ) * 100
		errory = graph_nominal.GetErrorY(i)
		errorx = graph_nominal.GetErrorX(i)


		graph_diff.SetPoint(i,x_nominal,diff)
		graph_diff.SetPointError(i,errorx/2,errorx/2,errory/2,errory/2)


	return graph_diff



def DivideGraphs(graph_mc, graph_data):

	n_points = graph_mc.GetN()
	graph_ratio = Graph(n_points-1)

	for i in range(0,n_points-1):
	    x_mc = ROOT.Double()
	    y_mc = ROOT.Double()
	    x_data = ROOT.Double()
	    y_data = ROOT.Double()
	    ratio = ROOT.Double()
	    errory = ROOT.Double()
	    errorx = ROOT.Double()
	    
	    mc = graph_mc.GetPoint(i,x_mc,y_mc)
	    data = graph_data.GetPoint(i,x_data,y_data)
	    ratio = y_data/y_mc
	    errory = graph_data.GetErrorY(i)
	    errorx = graph_data.GetErrorX(i)
	    
	    graph_ratio.SetPoint(i,x_mc,ratio)
	    graph_ratio.SetPointError(i,errorx/2,errorx/2,errory/2,errory/2)

	return graph_ratio




def MakeQuadSum(graph_dPhi,graph_subJet,graph_JVT,graph_sherpa, graph_phScale, graph_phRes, graph_jet1, graph_jet2, graph_jet3):

	n_points = graph_dPhi.GetN()
	hist_quad = Hist([0,80,110,150,200,250,300,350,400,450])


	for i in range(0,n_points):
		x_dPhi = ROOT.Double()
		y_dPhi = ROOT.Double()
		x_subJet = ROOT.Double()
		y_subJet = ROOT.Double()
		x_JVT = ROOT.Double()
		y_JVT = ROOT.Double()
		x_sherpa = ROOT.Double()
		y_sherpa = ROOT.Double()
		x_phScale = ROOT.Double()
		y_phScale = ROOT.Double()
		x_phRes = ROOT.Double()
		y_phRes = ROOT.Double()
		x_jet1 = ROOT.Double()
		y_jet1 = ROOT.Double()
		x_jet2 = ROOT.Double()
		y_jet2 = ROOT.Double()
		x_jet3 = ROOT.Double()
		y_jet3 = ROOT.Double()

		m_dPhi = graph_dPhi.GetPoint(i,x_dPhi,y_dPhi)
		m_subJet = graph_subJet.GetPoint(i,x_subJet,y_subJet)
		m_JVT = graph_JVT.GetPoint(i,x_JVT,y_JVT)
		m_sherpa = graph_sherpa.GetPoint(i,x_sherpa,y_sherpa)
		m_phScale = graph_phScale.GetPoint(i, x_phScale, y_phScale)
		m_phRes = graph_phRes.GetPoint(i, x_phRes, y_phRes)
		m_jet1 = graph_jet1.GetPoint(i, x_jet1, y_jet1)
		m_jet2 = graph_jet2.GetPoint(i, x_jet2, y_jet2)
		m_jet3 = graph_jet3.GetPoint(i, x_jet3, y_jet3)


		quad_sum = sqrt(y_dPhi**2 + y_subJet**2 + y_JVT**2 + y_sherpa**2 + y_phScale**2 + y_phRes**2 + y_jet1**2 + y_jet2**2 + y_jet3**2)

		hist_quad.Fill(x_dPhi,quad_sum)

	return hist_quad


def SystematicsPlot(graph_subJet, graph_dPhi, graph_JVT, graph_sherpa, graph_phScale, graph_phRes, graph_jet1, graph_jet2, graph_jet3, 
	hist_quad, eta_flag, updown_flag, btag_flag):

	canvas.Clear()

	hist_quad.GetYaxis().SetTitle("Difference from Nominal [%]")
	hist_quad.GetXaxis().SetTitle("P_{T}^{ref} [GeV]")

	hist_quad.GetYaxis().SetRangeUser(0,20)
	graph_subJet.GetYaxis().SetRangeUser(0,10)

	graph_subJet.markercolor = ROOT.kRed
	graph_subJet.linecolor = ROOT.kRed
	graph_dPhi.markercolor = ROOT.kBlue
	graph_dPhi.linecolor = ROOT.kBlue
	graph_JVT.markercolor = ROOT.kGreen+3
	graph_JVT.linecolor = ROOT.kGreen+3
	graph_sherpa.linecolor = ROOT.kMagenta
	graph_sherpa.markercolor = ROOT.kMagenta
	graph_phScale.linecolor = 
	graph_phScale.markercolor = 
	graph_phRes.linecolor = 
	graph_phRes.markercolor = 
	graph_jet1.linecolor = 
	graph_jet1.markercolor = 
	graph_jet2.linecolor = 
	graph_jet2.markercolor = 
	graph_jet3.linecolor = 
	graph_jet3.markercolor = 

	hist_quad.fillstyle = 'solid'
	hist_quad.linewidth = 0
	hist_quad.fillcolor = ROOT.kGray


	legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
	legend.AddEntry(graph_subJet, "Subleading Jet", style = 'p')
	legend.AddEntry(graph_dPhi, "dPhi", style = 'p')
	legend.AddEntry(graph_JVT, "JVT", style = 'p')
	legend.AddEntry(graph_sherpa, "Sherpa", style = 'p')
	legend.AddEntry(graph_phScale, "Photon Energy Scale", style = 'p')
	legend.AddEntry(graph_phRes, "Photon Energy Resolution", style = 'p')
	legend.AddEntry(graph_jet1, "JES/JER 1", style = 'p')
	legend.AddEntry(graph_jet2, "JES/JER 2", style = 'p')
	legend.AddEntry(graph_jet3, "JES/JER 3", style = 'p')
	legend.AddEntry(hist_quad, "Total", style = 'f')

	hist_quad.Draw("hist")
	graph_subJet.Draw("pelsame")
	graph_dPhi.Draw("pelsame")
	graph_JVT.Draw("pelsame")
	graph_sherpa.Draw("pelsame")
	graph_phScale.Draw("pelsame")
	graph_phRes.Draw("pelsame")
	graph_jet1.Draw("pelsame")
	graph_jet2.Draw("pelsame")
	graph_jet3.Draw("pelsame")
	legend.Draw("same")

	label = ROOT.TText(0.675, 0.7, 'ATLAS')
	label.SetTextFont(73)
	label.SetTextSize(25)
	label.SetNDC()
	label.Draw()

	label2 = ROOT.TText(0.775, 0.7, 'Internal')
	label2.SetTextFont(43)
	label2.SetTextSize(25)
	label2.SetNDC()
	label2.Draw()

	label3 = ROOT.TLatex(0.7, 0.65, eta_strings[eta_flag])
	label3.SetTextFont(43)
	label3.SetTextSize(20)
	label3.SetNDC()
	label3.Draw()

	label4 = ROOT.TLatex(0.7, 0.60, "systematics " + updown_flag)
	label4.SetTextFont(43)
	label4.SetTextSize(20)
	label4.SetNDC()
	label4.Draw()

	label5 = ROOT.TLatex(0.7, 0.55, btag_flag)
	label5.SetTextFont(43)
	label5.SetTextSize(20)
	label5.SetNDC()
	label5.Draw()

	canvas.Modified()
	canvas.Update()

	canvas.SaveAs(out_folder + "total_stats_" + eta_flag + "_" + updown_flag + "_" + btag_flag + ".pdf")




def MakeDifferences(btag_flag,updown_flag):
	pythia_nominal = root_open("root_files/pythia_nominal_mean_graphs_" + btag_flag + ".root")
	data_nominal = root_open("root_files/data_nominal_mean_graphs_" + btag_flag + ".root")

	#dPhi
	pythia_dPhi = root_open("root_files/pythia_dPhi_" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_dPhi = root_open("root_files/data_dPhi_" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#subjet
	pythia_subJet = root_open("root_files/pythia_subJet_" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_subJet = root_open("root_files/data_subJet_" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#JVT
	pythia_JVT = root_open("root_files/pythia_JVT_" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_JVT = root_open("root_files/data_JVT_" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	
	#ph scale 
	pythia_phScale = root_open("root_files/pythia_EG_SCALE_ALL_1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_phScale = root_open("root_files/data_JEG_SCALE_ALL_1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#ph res 
	pythia_phRes = root_open("root_files/pythia_EG_RESOLUTION_ALL_1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_phRes = root_open("root_files/data_RESOLUTION_ALL_1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#jet sys 1 
	pythia_jet1 = root_open("root_files/pythia_JET_GroupedNP_1__1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_jet1 = root_open("root_files/data_JET_GroupedNP_1__1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#jet sys 2
	pythia_jet2 = root_open("root_files/pythia_JET_GroupedNP_2__1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_jet2 = root_open("root_files/data_JET_GroupedNP_2__1" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#jet sys 3 
	pythia_jet3 = root_open("root_files/pythia_JET_GroupedNP_3__1up" +updown_flag + "_mean_graphs_" + btag_flag + ".root")
	data_jet3 = root_open("root_files/data_JET_GroupedNP_3__1up" +updown_flag + "_mean_graphs_" + btag_flag + ".root")

	#sherpa
	sherpa = root_open("root_files/sherpa_nominal_mean_graphs_" + btag_flag + ".root")

	# for eta in eta_vals:

	# 	graph_mc_nominal = pythia_nominal.Get(eta)
	# 	graph_data_nominal = data_nominal.Get(eta)
	# 	ratio_nominal = DivideGraphs(graph_mc_nominal,graph_data_nominal)

	# 	graph_mc_dPhi = pythia_dPhi.Get(eta)
	# 	graph_data_dPhi = data_dPhi.Get(eta)
	# 	ratio_dPhi = DivideGraphs(graph_mc_dPhi,graph_data_dPhi)
		
	# 	graph_mc_subJet = pythia_subJet.Get(eta)
	# 	graph_data_subJet = data_subJet.Get(eta)
	# 	ratio_subJet = DivideGraphs(graph_mc_subJet,graph_data_subJet)

	# 	graph_diff_dPhi = GetPercentDiffGraph(ratio_nominal,ratio_dPhi)
	# 	graph_diff_subJet = GetPercentDiffGraph(ratio_nominal,ratio_subJet)

	# 	graph_quad = MakeQuadSum(graph_diff_dPhi,graph_diff_subJet)

	# 	SystematicsPlot(graph_diff_subJet,graph_diff_dPhi,graph_quad,eta,updown_flag,btag_flag)

	eta = "0.0<|eta|<0.8"

	print eta

	#set ratio graphs
	graph_mc_nominal = pythia_nominal.Get(eta)
	graph_data_nominal = data_nominal.Get(eta)
	ratio_nominal = DivideGraphs(graph_mc_nominal,graph_data_nominal)

	graph_mc_dPhi = pythia_dPhi.Get(eta)
	graph_data_dPhi = data_dPhi.Get(eta)
	ratio_dPhi = DivideGraphs(graph_mc_dPhi,graph_data_dPhi)
	
	graph_mc_subJet = pythia_subJet.Get(eta)
	graph_data_subJet = data_subJet.Get(eta)
	ratio_subJet = DivideGraphs(graph_mc_subJet,graph_data_subJet)

	graph_mc_JVT = pythia_JVT.Get(eta)
	graph_data_JVT = data_JVT.Get(eta)
	ratio_JVT = DivideGraphs(graph_mc_JVT,graph_data_JVT)

	graph_mc_phScale = pythia_phScale.Get(eta)
	graph_data_phScale = data_phScale.Get(eta)
	ratio_phScale = DivideGraphs(graph_mc_phScale,graph_data_phScale)

	graph_mc_phRes = pythia_phRes.Get(eta)
	graph_data_phRes = data_phRes.Get(eta)
	ratio_phRes = DivideGraphs(graph_mc_phRes,graph_data_phRes)

	graph_mc_jet1 = pythia_jet1.Get(eta)
	graph_data_jet1 = data_jet1.Get(eta)
	ratio_jet1 = DivideGraphs(graph_mc_jet1,graph_data_jet1)

	graph_mc_jet2 = pythia_jet2.Get(eta)
	graph_data_jet2 = data_jet2.Get(eta)
	ratio_jet2 = DivideGraphs(graph_mc_jet2,graph_data_jet2)

	graph_mc_jet3 = pythia_jet3.Get(eta)
	graph_data_jet3 = data_jet3.Get(eta)
	ratio_jet3 = DivideGraphs(graph_mc_jet3,graph_data_jet3)

	graph_sherpa = sherpa.Get(eta)
	ratio_sherpa = DivideGraphs(graph_sherpa,graph_data_nominal)


	#divide them
	graph_diff_dPhi = GetPercentDiffGraph(ratio_nominal,ratio_dPhi)
	graph_diff_subJet = GetPercentDiffGraph(ratio_nominal,ratio_subJet)
	graph_diff_JVT = GetPercentDiffGraph(ratio_nominal,ratio_JVT)
	graph_diff_sherpa = GetPercentDiffGraph(ratio_nominal,ratio_sherpa)
	graph_diff_phScale = GetPercentDiffGraph(ratio_nominal,ratio_phScale)
	graph_diff_phRes = GetPercentDiffGraph(ratio_nominal,ratio_phRes)
	graph_diff_jet1 = GetPercentDiffGraph(ratio_nominal,ratio_jet1)
	graph_diff_jet2 = GetPercentDiffGraph(ratio_nominal,ratio_jet2)
	graph_diff_jet3 = GetPercentDiffGraph(ratio_nominal,ratio_jet3)


	graph_quad = MakeQuadSum(graph_diff_dPhi, graph_diff_subJet, graph_diff_JVT, graph_diff_sherpa, graph_diff_phRes, graph_diff_phRes, graph_diff_jet1, graph_diff_jet2, graph_diff_jet3)

	SystematicsPlot(graph_diff_subJet, graph_diff_dPhi, graph_diff_JVT, graph_diff_sherpa, graph_diff_phScale, graph_diff_phScale, graph_diff_jet1, graph_diff_jet2, graph_diff_jet3, graph_quad, eta, updown_flag, btag_flag)




MakeDifferences("inclusive","up")
MakeDifferences("isFix85","up")
MakeDifferences("isFix70","up")
MakeDifferences("inclusive","down")
MakeDifferences("isFix85","down")
MakeDifferences("isFix70","down")

