import rootpy.ROOT as ROOT
from rootpy.io import root_open
from rootpy.plotting import *
from rootpy.plotting.style import *
#from rootpy.plotting import Hist, Hist2D, Graph, Canvas, Legend, Color
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from rootpy.tree import Tree, TreeChain


pythia_inclusive = root_open("root_files/pythia_nominal_mean_graphs_inclusive.root")
pythia_isFix70 = root_open("root_files/pythia_nominal_mean_graphs_isFix70.root")
pythia_isFix85 = root_open("root_files/pythia_nominal_mean_graphs_isFix85.root")
pythia_truth = root_open("root_files/pythia_nominal_mean_graphs_truth.root")

data_inclusive = root_open("root_files/data_nominal_mean_graphs_inclusive.root")
data_isFix70 = root_open("root_files/data_nominal_mean_graphs_isFix70.root")
data_isFix85 = root_open("root_files/data_nominal_mean_graphs_isFix85.root")

pythia_light_inc = root_open("root_files/pythia_nominal_lightTruth_inc")
pythia_light_70 = root_open("root_files/pythia_nominal_lightTruth_70")
pythia_light_85 = root_open("root_files/pythia_nominal_lightTruth_85")

pythia_c_inc = root_open("root_files/pythia_nominal_cTruth_inc")
pythia_c_70 = root_open("root_files/pythia_nominal_cTruth_70")
pythia_c_85 = root_open("root_files/pythia_nominal_cTruth_85")

pythia_b_inc = root_open("root_files/pythia_nominal_bTruth_inc")
pythia_b_70 = root_open("root_files/pythia_nominal_bTruth_70")
pythia_b_85 = root_open("root_files/pythia_nominal_bTruth_85")

out_folder = "./outplot_systematics/"

set_style('ATLAS')

canvas = Canvas()

eta_vals = ['0.0<|eta|<0.8', '0.8<|eta|<2.0','|eta|>2.0']
eta_strings = eta_vals ={'0.0<|eta|<0.8' : '0 < |#eta| < 0.8',
	 '0.8<|eta|<2.0' : '0.8 < |#eta| < 2.0',
	 '|eta|>2.0' : '|#eta| > 2.0'}


def DrawBTag(file_data,file_pythia,b_tag,eta_flag):

	canvas.Clear()

	hist_pythia = file_pythia.Get(eta_flag)
	hist_data = file_data.Get(eta_flag)

	hist_data.xaxis.SetTitle("P_{T}^{ref} [GeV]")
	hist_data.yaxis.SetTitle("P_{T}^{jet} / P_{T}^{ref} Peak Value")
	hist_pythia.linecolor = ROOT.kBlue
	hist_pythia.markercolor = ROOT.kBlue

	hist_data.RemovePoint(8)
	hist_pythia.RemovePoint(8)
	hist_data.GetYaxis().SetRangeUser(.6,1.6)
	hist_data.GetXaxis().SetRangeUser(0,1000)

	legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
	legend.AddEntry(hist_pythia, "Pythia")
	legend.AddEntry(hist_data, "Data")

	hist_data.Draw("pea")
	hist_pythia.Draw("pesame")
	legend.Draw("same")

	label = ROOT.TText(0.675, 0.75, 'ATLAS')
	label.SetTextFont(73)
	label.SetTextSize(25)
	label.SetNDC()
	label.Draw()

	label2 = ROOT.TText(0.775, 0.75, 'Internal')
	label2.SetTextFont(43)
	label2.SetTextSize(25)
	label2.SetNDC()
	label2.Draw()

	label3 = ROOT.TLatex(0.7, 0.7, eta_strings[eta_flag])
	label3.SetTextFont(43)
	label3.SetTextSize(20)
	label3.SetNDC()
	label3.Draw()

	label4 = ROOT.TText(0.7,0.65, b_tag)
	label4.SetTextFont(43)
	label4.SetTextSize(20)
	label4.SetNDC()
	label4.Draw()

	canvas.Modified()
	canvas.Update()

	canvas.SaveAs(out_folder + b_tag + "_" + eta_flag + ".pdf")



for eta in eta_vals:

	DrawBTag(data_inclusive,pythia_inclusive,"inclusive",eta)
	DrawBTag(data_isFix70,pythia_isFix70,"isFix70",eta)
	DrawBTag(data_isFix85,pythia_isFix85,"isFix85",eta)



