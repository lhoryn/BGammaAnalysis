import rootpy.ROOT as ROOT
from rootpy.io import root_open
from rootpy.plotting import *
from rootpy.plotting.style import *
#from rootpy.plotting import Hist, Hist2D, Graph, Canvas, Legend, Color
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from rootpy.tree import Tree, TreeChain

#sherpa_inclusive = root_open("root_files/truthSherpa_mean_graphs_inclusive.root")
#sherpa_isFix70 = root_open("root_files/truthSherpa_mean_graphs_isFix70.root")
#sherpa_isFix85 = root_open("root_files/truthSherpa_mean_graphs_isFix85.root")


pythia_inclusive = root_open("root_files/pythia_nominal_mean_graphs_inclusive.root")
pythia_isFix70 = root_open("root_files/pythia_nominal_mean_graphs_isFix70.root")
pythia_isFix85 = root_open("root_files/pythia_nominal_mean_graphs_isFix85.root")
pythia_truth = root_open("root_files/pythia_nominal_mean_graphs_truth.root")

# pythia_inclusive = root_open("root_files/pythia_nominal_mean_graphs_lightTruth_inc.root")
# pythia_isFix70 = root_open("root_files/pythia_nominal_mean_graphs_lightTruth_70.root")
# pythia_isFix85 = root_open("root_files/pythia_nominal_mean_graphs_lightTruth_85.root")


data_inclusive = root_open("root_files/data_nominal_mean_graphs_inclusive.root")
data_isFix70 = root_open("root_files/data_nominal_mean_graphs_isFix70.root")
data_isFix85 = root_open("root_files/data_nominal_mean_graphs_isFix85.root")

pythia_light_inc = root_open("root_files/pythia_nominal_mean_graphs_lightTruth_inc.root")
pythia_light_70 = root_open("root_files/pythia_nominal_mean_graphs_lightTruth_70.root")
pythia_light_85 = root_open("root_files/pythia_nominal_mean_graphs_lightTruth_85.root")

pythia_c_inc = root_open("root_files/pythia_nominal_mean_graphs_cTruth_inc.root")
pythia_c_70 = root_open("root_files/pythia_nominal_mean_graphs_cTruth_70.root")
pythia_c_85 = root_open("root_files/pythia_nominal_mean_graphs_cTruth_85.root")

pythia_b_inc = root_open("root_files/pythia_nominal_mean_graphs_bTruth_inc.root")
pythia_b_70 = root_open("root_files/pythia_nominal_mean_graphs_bTruth_70.root")
pythia_b_85 = root_open("root_files/pythia_nominal_mean_graphs_bTruth_85.root")

out_folder = "./outplot_systematics/"

set_style('ATLAS')

canvas = Canvas()

eta_vals = ['0.0<|eta|<0.8', '0.8<|eta|<2.0','|eta|>2.0']
eta_strings = eta_vals ={'0.0<|eta|<0.8' : '0 < |#eta| < 0.8',
	 '0.8<|eta|<2.0' : '0.8 < |#eta| < 2.0',
	 '|eta|>2.0' : '|#eta| > 2.0'}


def DrawNoTruth(file_inclusive,file_isFix70,file_isFix85,data_flag,eta_flag):

		canvas.Clear()


		hist_inclusive = file_inclusive.Get(eta_flag)
		hist_isFix70 = file_isFix70.Get(eta_flag)
		hist_isFix85 = file_isFix85.Get(eta_flag)

		hist_inclusive.xaxis.SetTitle("P_{T}^{ref} [GeV]")
		hist_inclusive.yaxis.SetTitle("P_{T}^{jet} / P_{T}^{ref} Peak Value")
		hist_isFix85.markercolor = ROOT.kViolet-3
		hist_isFix85.linecolor = ROOT.kViolet-3
		hist_isFix70.linecolor = ROOT.kGreen+2
		hist_isFix70.markercolor = ROOT.kGreen+2

		#hist_isFix85.RemovePoint(8)
		#hist_isFix70.RemovePoint(8)
		#hist_inclusive.RemovePoint(8)
		hist_inclusive.GetYaxis().SetRangeUser(.6,1.6)
		hist_inclusive.GetXaxis().SetRangeUser(170,950)



		legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
		legend.AddEntry(hist_inclusive, "Inclusive")
		legend.AddEntry(hist_isFix70, "isFix70")
		legend.AddEntry(hist_isFix85, "isFix85")

		hist_inclusive.Draw("pea")
		hist_isFix70.Draw("pesame")
		hist_isFix85.Draw("pesame")
		legend.Draw("same")

		label = ROOT.TText(0.675, 0.75, 'ATLAS')
		label.SetTextFont(73)
		label.SetTextSize(25)
		label.SetNDC()
		label.Draw()

		label2 = ROOT.TText(0.775, 0.75, 'Internal')
		label2.SetTextFont(43)
		label2.SetTextSize(25)
		label2.SetNDC()
		label2.Draw()

		# label5 = ROOT.TLatex(0.65, 0.60, "Truth Tagged Light")
		# label5.SetTextFont(43)
		# label5.SetTextSize(25)
		# label5.SetNDC()
		# label5.Draw()


		label3 = ROOT.TLatex(0.7, 0.7, eta_strings[eta_flag])
		label3.SetTextFont(43)
		label3.SetTextSize(20)
		label3.SetNDC()
		label3.Draw()

		label4 = ROOT.TText(0.7,0.65, data_flag)
		label4.SetTextFont(43)
		label4.SetTextSize(20)
		label4.SetNDC()
		label4.Draw()

		canvas.Modified()
		canvas.Update()

		canvas.SaveAs(out_folder + data_flag + "_" + eta_flag + ".pdf")


def DrawTruth(file_inclusive,file_isFix70,file_isFix85,file_truth,data_flag,eta_flag):

		canvas.Clear()

		hist_inclusive = file_inclusive.Get(eta_flag)
		hist_isFix70 = file_isFix70.Get(eta_flag)
		hist_isFix85 = file_isFix85.Get(eta_flag)
		hist_truth = file_truth.Get(eta_flag)

		hist_inclusive.xaxis.SetTitle("P_{T}^{ref} [GeV]")
		hist_inclusive.yaxis.SetTitle("P_{T}^{jet} / P_{T}^{ref} Peak Value")
		hist_isFix85.markercolor = ROOT.kViolet-3
		hist_isFix85.linecolor = ROOT.kViolet-3
		hist_isFix70.linecolor = ROOT.kGreen+2
		hist_isFix70.markercolor = ROOT.kGreen+2
		hist_truth.linecolor = ROOT.kRed +2
		hist_truth.markercolor = ROOT.kRed +2

		#hist_isFix85.RemovePoint(8)
		#hist_isFix70.RemovePoint(8)
		#hist_inclusive.RemovePoint(8)
		hist_truth.RemovePoint(8)
		hist_inclusive.GetYaxis().SetRangeUser(.7,1.3)
		hist_inclusive.GetXaxis().SetRangeUser(170,950)


		legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
		legend.AddEntry(hist_inclusive, "Inclusive")
		legend.AddEntry(hist_isFix70, "isFix70")
		legend.AddEntry(hist_isFix85, "isFix85")
		legend.AddEntry(hist_truth, "truth")

		hist_inclusive.Draw("pea")
		hist_isFix70.Draw("pesame")
		hist_isFix85.Draw("pesame")
		hist_truth.Draw("pesame")
		legend.Draw("same")

		label = ROOT.TText(0.675, 0.75, 'ATLAS')
		label.SetTextFont(73)
		label.SetTextSize(25)
		label.SetNDC()
		label.Draw()

		label2 = ROOT.TText(0.775, 0.75, 'Internal')
		label2.SetTextFont(43)
		label2.SetTextSize(25)
		label2.SetNDC()
		label2.Draw()

		label3 = ROOT.TLatex(0.7, 0.7, eta_strings[eta])
		label3.SetTextFont(43)
		label3.SetTextSize(20)
		label3.SetNDC()
		label3.Draw()

		label4 = ROOT.TText(0.7,0.65, data_flag)
		label4.SetTextFont(43)
		label4.SetTextSize(20)
		label4.SetNDC()
		label4.Draw()

		canvas.Modified()
		canvas.Update()

		canvas.SaveAs(out_folder + data_flag + "_" + eta_flag + ".pdf")



for eta in eta_vals:

	DrawNoTruth(data_inclusive,data_isFix70,data_isFix85, "Data",eta)
	DrawTruth(pythia_inclusive,pythia_isFix70,pythia_isFix85,pythia_truth,"Pythia",eta)
#DrawNoTruth(sherpa_inclusive,sherpa_isFix70,sherpa_isFix85,"Sherpa",eta)
	DrawNoTruth(pythia_light_inc,pythia_light_70,pythia_light_85,"B-Tagged, True Light Flavor",eta)
	DrawNoTruth(pythia_c_inc,pythia_c_70,pythia_c_85,"B-Tagged, True Charm",eta)
	DrawNoTruth(pythia_b_inc,pythia_b_70,pythia_b_85,"B-Tagged, True Bottom",eta)




