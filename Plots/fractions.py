import rootpy.ROOT as ROOT
from rootpy.io import root_open
from rootpy.plotting import *
from rootpy.plotting.style import *
#from rootpy.plotting import Hist, Hist2D, Graph, Canvas, Legend, Color
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.io as io
from rootpy.plotting.views import *

#full pythia 
pythia_inclusive = root_open("root_files/pythia_nominal_output_inclusive.root")
pythia_isFix70 = root_open("root_files/pythia_nominal_output_isFix70.root")
pythia_isFix85 = root_open("root_files/pythia_nominal_output_isFix85.root")

#stacking
inclusive_light = root_open("root_files/pythia_nominal_output_lightTruth_inc.root")
inclusive_b = root_open("root_files/pythia_nominal_output_bTruth_inc.root")
inclusive_c = root_open("root_files/pythia_nominal_output_cTruth_inc.root")

isFix70_light = root_open("root_files/pythia_nominal_output_lightTruth_70.root")
isFix70_b = root_open("root_files/pythia_nominal_output_bTruth_70.root")
isFix70_c = root_open("root_files/pythia_nominal_output_cTruth_70.root")

isFix85_light = root_open("root_files/pythia_nominal_output_lightTruth_85.root")
isFix85_b = root_open("root_files/pythia_nominal_output_bTruth_85.root")
isFix85_c = root_open("root_files/pythia_nominal_output_cTruth_85.root")

outFolder = "./outplot_systematics/"

set_style('ATLAS')

canvas = Canvas()

eta_range = ['00','01','02']
pt_range = ['00','01','02','03','04','05','06','07','08']

pt_vals = {'00' : '0 < p_{T}^{jet} [GeV] < 80', 
	'01' : '80 < p_{T}^{jet} [GeV] < 110', 
	'02' : '110 < p_{T}^{jet} [GeV] < 150',
	'03' : '150 < p_{T}^{jet} [GeV] < 200',
	'04' : '200 < p_{T}^{jet} [GeV] < 250',
	'05' : '250 < p_{T}^{jet} [GeV] < 300', 
	'06' : '300 < p_{T}^{jet} [GeV] < 350', 
	'07' : '350 < p_{T}^{jet} [GeV] < 400',
	'08': 'p_{T}^{jet} [GeV] > 400' }

pt_bin = {'00':0, '01':80, '02':110, '03':150, '04':200, '05':250, '06':300, '07':350, '08':400}

eta_vals ={'00' : '0 < |#eta| < 0.8',
	 '01' : '0.8 < |#eta| < 2.0',
	 '02' : '|#eta| > 2.0'}


def StackAndPrint(hist_light,hist_c,hist_b,eta,b_tag):

	canvas.Clear()

	hist_light.linecolor = ROOT.kCyan+2
	hist_light.lineweight = 2
	hist_c.linecolor = ROOT.kBlue-2
	hist_c.lineweight = 2
	hist_b.linecolor = ROOT.kViolet-3
	hist_b.lineweight = 2

	hist_light.xaxis.SetTitle("P_{T}^{jet}")
	hist_light.yaxis.SetTitle("Flavor Fraction")
	hist_light.yaxis.SetRangeUser(0,1)

	legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
	legend.AddEntry(hist_light, "Light jets", style = 'l')
	legend.AddEntry(hist_c,"c jets", style = 'l')
	legend.AddEntry(hist_b, "b jets", style = 'l')

	hist_light.Draw("hist")
	hist_c.Draw("histsame")
	hist_b.Draw("histsame")
	legend.Draw("same")

	label = ROOT.TText(0.675, 0.7, 'ATLAS')
	label.SetTextFont(73)
	label.SetTextSize(25)
	label.SetNDC()
	label.Draw()

	label2 = ROOT.TText(0.775, 0.7, 'Internal')
	label2.SetTextFont(43)
	label2.SetTextSize(25)
	label2.SetNDC()
	label2.Draw()

	label3 = ROOT.TLatex(0.7, 0.65, eta_vals[eta])
	label3.SetTextFont(43)
	label3.SetTextSize(20)
	label3.SetNDC()
	label3.Draw()

	label4 = ROOT.TLatex(0.7,0.6, b_tag)
	label4.SetTextFont(43)
	label4.SetTextSize(20)
	label4.SetNDC()
	label4.Draw()

	canvas.Modified()
	canvas.Update()

	canvas.SaveAs(outFolder + "fraction_" + eta + "_" + b_tag + ".pdf")






def DrawFractions(file_light,file_c,file_b,file_mc,eta,flag):

	#adjust for stupid naming choice
	if flag == "inclusive":
		file_flag = "inc"
	if flag == "isFix70":
		file_flag = "70"
	if flag == "isFix85":
		file_flag = "85"

	#9 points, one for each pt bin
	hist_frac_light = Hist([0,80,110,150,200,250,300,350,400,450])
	hist_frac_c = Hist([0,80,110,150,200,250,300,350,400,450])
	hist_frac_b = Hist([0,80,110,150,200,250,300,350,400,450])


	for pt in pt_range:

		#set truth content stack
		hist_light = file_light.Get('jvtpythia_nominal_Eta' + eta + '_Pt' + pt + '_lightTruth_' + file_flag )
		hist_c = file_c.Get('jvtpythia_nominal_Eta' + eta + '_Pt' + pt + '_cTruth_' + file_flag)
		hist_b = file_b.Get('jvtpythia_nominal_Eta' + eta + '_Pt' + pt + '_bTruth_' + file_flag)
	
		#draw with data and MC
		hist_mc = file_mc.Get('jvtpythia_nominal_Eta' + eta + '_Pt' + pt + '_' + flag)
		
		frac_light = (hist_light.Integral() / hist_mc.Integral())
		hist_frac_light.Fill(pt_bin[pt], frac_light)

		frac_c = (hist_c.Integral() / hist_mc.Integral())
		hist_frac_c.Fill(pt_bin[pt], frac_c)

		frac_b = (hist_b.Integral() / hist_mc.Integral())
		hist_frac_b.Fill(pt_bin[pt], frac_b)

	StackAndPrint(hist_frac_light,hist_frac_c,hist_frac_b,eta,flag)






for eta in eta_range:

	#Draw inclusive
	DrawFractions(inclusive_light,inclusive_c,inclusive_b,pythia_inclusive,eta,"inclusive")

	#Draw isFix70
	DrawFractions(isFix70_light,isFix70_c,isFix70_b,pythia_isFix70,eta,"isFix70")

	#Draw isFix85
	DrawFractions(isFix85_light,isFix85_c,isFix85_b,pythia_isFix85,eta,"isFix85")






