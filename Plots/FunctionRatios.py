import rootpy.ROOT as ROOT
from rootpy.io import root_open
from rootpy.plotting import *
from rootpy.plotting.style import *
#from rootpy.plotting import Hist, Hist2D, Graph, Canvas, Legend, Color
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from rootpy.tree import Tree, TreeChain


pythia_isFix85 = root_open("root_files/pythia_nominal_mean_graphs_isFix85.root")
data_isFix85 = root_open("root_files/data_nominal_mean_graphs_isFix85.root")

pythia_inclusive = root_open("root_files/pythia_nominal_mean_graphs_inclusive.root")
data_inclusive = root_open("root_files/data_nominal_mean_graphs_inclusive.root")

pythia_isFix70 = root_open("root_files/pythia_nominal_mean_graphs_isFix70.root")
data_isFix70 = root_open("root_files/data_nominal_mean_graphs_isFix70.root")




out_folder = "./outplot_systematics/"

set_style('ATLAS')

canvas = Canvas()

eta_vals = ['0.0<|eta|<0.8', '0.8<|eta|<2.0','|eta|>2.0']
eta_strings = eta_vals ={'0.0<|eta|<0.8' : '0 < |#eta| < 0.8',
	 '0.8<|eta|<2.0' : '0.8 < |#eta| < 2.0',
	 '|eta|>2.0' : '|#eta| > 2.0'}

def DivideGraphs(graph_mc, graph_data):

	n_points = graph_mc.GetN()
	graph_ratio = Graph(n_points-1)

	for i in range(0,n_points-1):
	    x_mc = ROOT.Double()
	    y_mc = ROOT.Double()
	    x_data = ROOT.Double()
	    y_data = ROOT.Double()
	    ratio = ROOT.Double()
	    errory = ROOT.Double()
	    errorx = ROOT.Double()
	    
	    mc = graph_mc.GetPoint(i,x_mc,y_mc)
	    data = graph_data.GetPoint(i,x_data,y_data)
	    ratio = y_data/y_mc
	    errory = graph_data.GetErrorY(i)
	    errorx = graph_data.GetErrorX(i)
	    
	    graph_ratio.SetPoint(i,x_mc,ratio)
	    graph_ratio.SetPointError(i,errorx/2,errorx/2,errory/2,errory/2)

	return graph_ratio


def StackGraphs(graph_inclusive,graph_isFix70,graph_isFix85,eta_flag):

	canvas.Clear()

	graph_inclusive.GetYaxis().SetTitle("Data/MC")
	graph_inclusive.GetXaxis().SetTitle("P_{T}^{ref} [GeV]")

	graph_inclusive.GetYaxis().SetRangeUser(.7,1.4)

	graph_isFix85.markercolor = ROOT.kViolet-3
	graph_isFix85.linecolor = ROOT.kViolet-3
	graph_isFix70.markercolor = ROOT.kGreen+2
	graph_isFix70.linecolor = ROOT.kGreen+2


	legend = Legend(2, leftmargin = 0.55, topmargin = 0.025, rightmargin = 0.01, textsize = 20 )
	legend.AddEntry(graph_inclusive, "Inclusive")
	legend.AddEntry(graph_isFix70, "isFix70")
	legend.AddEntry(graph_isFix85, "isFix85")

	graph_inclusive.Draw("pela")
	graph_isFix70.Draw("pelsame")
	graph_isFix85.Draw("pelsame")
	legend.Draw("same")

	label = ROOT.TText(0.675, 0.7, 'ATLAS')
	label.SetTextFont(73)
	label.SetTextSize(25)
	label.SetNDC()
	label.Draw()

	label2 = ROOT.TText(0.775, 0.7, 'Internal')
	label2.SetTextFont(43)
	label2.SetTextSize(25)
	label2.SetNDC()
	label2.Draw()

	label3 = ROOT.TLatex(0.7, 0.65, eta_strings[eta_flag])
	label3.SetTextFont(43)
	label3.SetTextSize(20)
	label3.SetNDC()
	label3.Draw()

	canvas.Modified()
	canvas.Update()

	canvas.SaveAs(out_folder + "ratio" + eta_flag + ".pdf")



for eta in eta_vals:

	graph_mc_inclusive = pythia_inclusive.Get(eta)
	graph_data_inclusive = data_inclusive.Get(eta)
	ratio_inclusive = DivideGraphs(graph_mc_inclusive,graph_data_inclusive)

	graph_mc_isFix70 = pythia_isFix70.Get(eta)
	graph_data_isFix70 = data_isFix70.Get(eta)
	ratio_isFix70 = DivideGraphs(graph_mc_isFix70,graph_data_isFix70)

	graph_mc_isFix85 = pythia_isFix85.Get(eta)
	graph_data_isFix85 = data_isFix85.Get(eta)
	ratio_isFix85 = DivideGraphs(graph_mc_isFix85,graph_data_isFix85)

	StackGraphs(ratio_inclusive,ratio_isFix70,ratio_isFix85,eta)












