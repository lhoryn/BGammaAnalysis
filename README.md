Code for b-jet+gamma JES analysis


To setup:

	setupATLAS
	lsetup root
	lsetup boost
	
if running from grid:

	voms-proxy-init -voms atlas
	lsetup fax
	lsetup rucio


paths to input files both locally and on the grid in directory locations/ 

MyFitter.cxx contains the main loop
